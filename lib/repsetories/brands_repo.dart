import 'package:flutter/material.dart';
import 'package:marche/data/dio/dioClient.dart';
import 'package:marche/data/response/api_response.dart';
import 'package:marche/data/response/config_model.dart';
import 'package:marche/excption/api_error_handler.dart';

class BrandRepo {
  final DioClient dioClient;

  BrandRepo({@required this.dioClient});
  Future<ApiResponse> getbrand() async {
    try {
      final response = await dioClient.get('${BaseUrls.ProductAPi}/getBrands');
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }
}
