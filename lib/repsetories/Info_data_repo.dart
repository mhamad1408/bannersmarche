import 'package:flutter/material.dart';
import 'package:marche/data/dio/dioClient.dart';
import 'package:marche/data/response/api_response.dart';
import 'package:marche/data/response/config_model.dart';
import 'package:marche/excption/api_error_handler.dart';

class InfoDataRepo {
  final DioClient dioClient;

  InfoDataRepo({@required this.dioClient});
  Future<ApiResponse> getinfo() async {
    try {
      final response =
          await dioClient.get('${BaseUrls.ProductAPi}/getInfoData');
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }
}
