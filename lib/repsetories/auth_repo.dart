import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:marche/data/dio/dioClient.dart';
import 'package:marche/data/response/api_response.dart';
import 'package:marche/data/response/config_model.dart';
import 'package:marche/excption/api_error_handler.dart';
import 'package:marche/model/client.dart';
import 'package:marche/util/app_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthRepo {
  final DioClient dioClient;
  final SharedPreferences sharedPreferences;

  AuthRepo({@required this.dioClient, @required this.sharedPreferences});

  Future<ApiResponse> login(ClientModel c) async {
    try {
      print(c.email);

      Response response = await dioClient.post(
        //'https://lemarche.verozonetesting.com/api/login',
        BaseUrls.ProductAPi + AppConstants.LoginURI,
        data: {"username": c.username, "password": c.password},
      );
      print(response);

      return ApiResponse.withSuccess(response);
    } catch (e) {
      print(e);

      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<void> saveUserToken(String token) async {
    dioClient.token = token;
    dioClient.dio.options.headers = {
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer $token'
    };
    try {
      await sharedPreferences.setString(AppConstants.TOKEN, token);
    } catch (e) {
      throw e;
    }
  }

  bool isLogin() {
    try {
      return sharedPreferences.get(AppConstants.TOKEN) != null ? true : false;
    } catch (e) {
      throw e;
    }
  }

  Future<ApiResponse> register(ClientModel c) async {
    try {
      Response response = await dioClient.post(
        BaseUrls.ProductAPi + AppConstants.RegisterURI,
        data: {
          "name": c.name,
          "phone": c.phone,
          "email": c.email,
          "username": c.username,
          "password": c.password
        },
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }
}
