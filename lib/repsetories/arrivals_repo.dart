import 'package:flutter/material.dart';
import 'package:marche/data/dio/dioClient.dart';
import 'package:marche/data/response/api_response.dart';
import 'package:marche/data/response/config_model.dart';
import 'package:marche/excption/api_error_handler.dart';

class ArrivalRepo {
  final DioClient dioClient;

  ArrivalRepo({@required this.dioClient});
  Future<ApiResponse> getarrival() async {
    try {
      final response =
          await dioClient.get('${BaseUrls.ProductAPi}/getNewArrivals');
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }
}
