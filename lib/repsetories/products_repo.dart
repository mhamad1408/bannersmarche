import 'package:flutter/material.dart';
import 'package:marche/data/dio/dioClient.dart';
import 'package:marche/data/response/api_response.dart';
import 'package:marche/data/response/config_model.dart';
import 'package:marche/excption/api_error_handler.dart';

class ProductsRepo {
  final DioClient dioClient;

  ProductsRepo({@required this.dioClient});
  Future<ApiResponse> getproducts(int subcategoryId) async {
    try {
      final response = await dioClient.get(
          '${BaseUrls.ProductAPi}/getProductSub?pageNum=0&pageSize=10&direction=asc&orderBy=product_tbl.product_id&subcategory_id=$subcategoryId');
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }
}
