import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:marche/controller/home_controller.dart';
import 'package:marche/provider/home_provider/arrivals_provider.dart';
import 'package:marche/provider/home_provider/auth_provider.dart';
import 'package:marche/provider/home_provider/banner_provider.dart';
import 'package:marche/provider/home_provider/brand_provider.dart';
import 'package:marche/provider/home_provider/cart_provider.dart';
import 'package:marche/provider/home_provider/category_provider.dart';
import 'package:marche/provider/home_provider/infodata_provider.dart';
import 'package:marche/provider/home_provider/products_provider.dart';
import 'package:marche/provider/home_provider/related_products_provider.dart';
import 'package:marche/provider/home_provider/slides_provider.dart';
import 'package:marche/provider/home_provider/specials_provider.dart';
import 'package:marche/screen/CheckoutScreen/CheckoutScreen.dart';
import 'package:marche/screen/Search/search.dart';
import 'package:marche/screen/Sign_in/log.dart';
//import 'package:marche/screen/categories/categories.dart';
import 'package:marche/screen/category/category.dart';
import 'package:marche/screen/home/home.dart';
import 'package:provider/provider.dart';
import 'di_container.dart' as di;
import 'model/cart.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await di.init();

  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (context) => di.sl<BannerProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<BrandProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<CategoryProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<ArrivalProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<SpecialsProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<SlidesProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<InfoDataProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<ProductsProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<AuthProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<CartProvider>()),
      ChangeNotifierProvider(
          create: (context) => di.sl<RelatedProdcutsProvider>()),
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  static final String title = 'User Profile';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.white,
        dividerColor: Colors.black,
      ),
      home: MyHomePage(),
      // routes: {
      // 'Categories' : (context){
      //   return Categories();
      // }
      // },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  void initState() {
    super.initState();
    _tabController = TabController(length: 5, vsync: this);
  }

  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: TabBarView(
        children: <Widget>[
          Home(),
          Category(),
          Search(),
          LoginScreen(),
          CheckoutPage(),
        ],
        controller: _tabController,
      ),
      bottomNavigationBar: Container(
        // padding: EdgeInsets.all(16.0),
        child: ClipRRect(
          //  borderRadius: BorderRadius.all(Radius.circular(8.0),
          //  ),
          child: Container(
            color: Colors.white70,
            child: TabBar(
              labelColor: Colors.blue,
              unselectedLabelColor: Colors.black54,
              labelStyle: TextStyle(fontSize: 8.0),
              indicator: UnderlineTabIndicator(
                borderSide: BorderSide(color: Colors.red, width: 0.0),
                insets: EdgeInsets.fromLTRB(50, 0, 50, 0),
              ),
              indicatorColor: Colors.yellowAccent,
              tabs: <Widget>[
                Tab(
                  icon: Icon(
                    Icons.home,
                    size: 24,
                  ),
                  child:
                      Text('Home', style: TextStyle(color: Colors.blueAccent)),
                ),
                Tab(
                  icon: Icon(
                    Icons.category,
                    size: 24,
                  ),
                  child: Text('Categories',
                      style: TextStyle(color: Colors.blueAccent)),
                ),
                Tab(
                  icon: Icon(
                    Icons.search,
                    size: 24,
                  ),
                  child: Text('Search',
                      style: TextStyle(color: Colors.blueAccent)),
                ),
                Tab(
                  icon: Icon(
                    Icons.person_outline_outlined,
                    size: 24,
                  ),
                  child: Text('profile',
                      style: TextStyle(color: Colors.blueAccent)),
                ),
                Tab(
                  icon: Icon(
                    Icons.add_shopping_cart,
                    size: 24,
                  ),
                  child: Text(
                    'cart',
                    style: TextStyle(color: Colors.blueAccent),
                  ),
                ),
              ],
              controller: _tabController,
            ),
          ),
        ),
      ),
    );
  }
}
