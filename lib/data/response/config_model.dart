class BaseUrls {
  static const String BaseAPi = "https://lemarche.verozonetesting.com/api";

  static const String LocalAPi = "http://localhost:8000/api/";

  static const String baseURL = "https://lemarche.verozonetesting.com";

  static const String ProductAPi = BaseAPi;

  static const String filesUrl = baseURL + "/";
}
