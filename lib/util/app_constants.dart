class AppConstants {
  static const String OnBoardingURI = 'onBoarding/list';
  static const String PushNotificationURI = 'notifications/list';
  static const String RoutelistMarkersURI = 'routeDetails/listMarkers/';
  static const String LoginURI = '/login';
  static const String RegisterURI = '/register';

  // Shared Key
  static const String MAP_KEY = 'AIzaSyAOQn_aSrh-djMUJblMIuFYKrULstRXn-U';
  static const String TOKEN = 'token';
  static const String TOPIC = 'greenline';
  static const String USER_TYPE = 'user_type';
  static const String USER_ID = '';
}
