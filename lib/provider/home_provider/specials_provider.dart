import 'package:flutter/material.dart';
import 'package:marche/data/response/api_response.dart';
import 'package:marche/model/produts.dart';
import 'package:marche/model/specials.dart';
import 'package:marche/repsetories/specials_repo.dart';

class SpecialsProvider extends ChangeNotifier {
  SpecialsRepo specialsRepo;

  SpecialsProvider({@required this.specialsRepo});
  List<ProductModel> _specialsList;
  List<ProductModel> get specialsList => _specialsList;
  Future<void> getSpecials(BuildContext context, bool reload) async {
    if (_specialsList == null || reload) {
      ApiResponse apiResponse = await specialsRepo.getspecials();
      if (apiResponse.response != null &&
          apiResponse.response.statusCode == 200) {
        _specialsList = [];
        apiResponse.response.data['data'].forEach((x) {
          ProductModel arrivalModel = ProductModel.fromJson(x);
          _specialsList.add(arrivalModel);
        });
      } else {
        print('error: ' + apiResponse.response.data);
        // //ApiChecker.checkApi(context, apiResponse);
      }
    }
    notifyListeners();
  }
}
