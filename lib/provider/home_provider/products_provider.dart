import 'package:flutter/material.dart';
import 'package:marche/data/response/api_response.dart';
import 'package:marche/model/produts.dart';
import 'package:marche/repsetories/products_repo.dart';

class ProductsProvider extends ChangeNotifier {
  ProductsRepo productsRepo;
  ProductsProvider({@required this.productsRepo});
  List<ProductModel> _productsList;
  List<ProductModel> get productList => _productsList;

  Future<void> getProducts(
    BuildContext context,
    bool reload,
    int subcategoryId,
  ) async {
    if (_productsList == null || reload) {
      ApiResponse apiResponse = await productsRepo.getproducts(
        subcategoryId,
      );
      if (apiResponse.response != null &&
          apiResponse.response.statusCode == 200) {
        _productsList = [];
        apiResponse.response.data['data'].forEach((x) {
          ProductModel productsModel = ProductModel.fromJson(x);
          _productsList.add(productsModel);
          print(x);
        });
        notifyListeners();
      } else {
        print('error: ' + apiResponse.response.data);
        // //ApiChecker.checkApi(context, apiResponse);
      }
    }
    notifyListeners();
  }
}
