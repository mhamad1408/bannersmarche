import 'package:flutter/material.dart';
import 'package:marche/data/response/api_response.dart';
import 'package:marche/model/arrivals.dart';
import 'package:marche/model/produts.dart';
import 'package:marche/repsetories/arrivals_repo.dart';

class ArrivalProvider extends ChangeNotifier {
  ArrivalRepo arrivalRepo;

  ArrivalProvider({@required this.arrivalRepo});
  List<ProductModel> _arrivalList;
  List<ProductModel> get arrivalList => _arrivalList;
  Future<void> getArrival(BuildContext context, bool reload) async {
    if (_arrivalList == null || reload) {
      ApiResponse apiResponse = await arrivalRepo.getarrival();
      if (apiResponse.response != null &&
          apiResponse.response.statusCode == 200) {
        _arrivalList = [];
        apiResponse.response.data['data'].forEach((x) {
          ProductModel arrivalModel = ProductModel.fromJson(x);
          _arrivalList.add(arrivalModel);
        });
      } else {
        print('error: ' + apiResponse.response.data);
        // //ApiChecker.checkApi(context, apiResponse);
      }
    }
    notifyListeners();
  }
}
