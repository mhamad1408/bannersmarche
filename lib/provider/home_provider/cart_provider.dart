import 'package:flutter/material.dart';
import 'package:marche/model/cart.dart';
import 'package:marche/repsetories/cart_repo.dart';

class CartProvider extends ChangeNotifier {
  final CartRepo cartRepo;
  CartProvider({@required this.cartRepo});

  double total = 0;
  double get _total => total;
  List<CartModel> _cartList = [];
  List<CartModel> get cartList => _cartList;

  void getCartData() {
    _cartList = [];
    _cartList.addAll(cartRepo.getCartList());
    _cartList.forEach((element) {
      total = _total + (element.price * element.qty);
    });
  }

  void addToCart(CartModel cartModel, int index) {
    if (index != null) {
      total = total - (_cartList[index].price * _cartList[index].qty);
      _cartList.replaceRange(index, index + 1, [cartModel]);
    } else {
      _cartList.add(cartModel);
    }
    total = total + (cartModel.price * cartModel.qty);
    cartRepo.addToCartList(_cartList);
    notifyListeners();
  }

  void removeFromCart(CartModel cartModel) {
    _cartList.remove(cartModel);
    total = total + (cartModel.price * cartModel.qty);
    cartRepo.addToCartList(_cartList);
    notifyListeners();
  }

  void clearAllCart() {
    _cartList = [];
    total = 0;
    cartRepo.addToCartList(_cartList);
    notifyListeners();
  }

  void setQuantity(bool isIncrement, CartModel cart) {
    int index = _cartList.indexOf(cart);
    if (isIncrement) {
      _cartList[index].quantity = _cartList[index].qty + 1;
      total = total + _cartList[index].price;
    } else {
      _cartList[index].quantity = _cartList[index].qty - 1;
      total = total - _cartList[index].price;
    }
    cartRepo.addToCartList(_cartList);

    notifyListeners();
  }
}
