import 'package:flutter/material.dart';
import 'package:marche/data/response/api_response.dart';
import 'package:marche/model/client.dart';
import 'package:marche/model/response_model.dart';
import 'package:marche/repsetories/auth_repo.dart';

class AuthProvider with ChangeNotifier {
  final AuthRepo authRepo;

  AuthProvider({@required this.authRepo});

  // for registration section
  bool _isLoading = false;

  bool get isLoading => _isLoading;

  String _loginErrorMessage = '';

  String get loginErrorMessage => _loginErrorMessage;

  Future<ResponseModel> login(ClientModel c) async {
    _isLoading = true;
    _loginErrorMessage = '';
    notifyListeners();
    ApiResponse apiResponse = await authRepo.login(c);
    ResponseModel responseModel;
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      Map map = apiResponse.response.data;
      String username = map["username"];
      // ignore: unused_local_variable
      String id = map["id"].toString();
      authRepo.saveUserToken(username);
      responseModel = ResponseModel(true, 'Success');
    } else {
      _isLoading = false;

      String errorMessage;
      if (apiResponse.error is String) {
        errorMessage = apiResponse.error.toString();
      } else {
        errorMessage = apiResponse.error.errors[0].message;
      }
      // print(errorMessage);
      _loginErrorMessage = errorMessage;
      responseModel = ResponseModel(false, errorMessage);
    }
    _isLoading = false;
    notifyListeners();
    return responseModel;
  }

  bool isLoggedIn() {
    return authRepo.isLogin();
  }

  Future<ResponseModel> register(ClientModel c) async {
    _isLoading = true;
    _loginErrorMessage = '';
    notifyListeners();
    ApiResponse apiResponse = await authRepo.register(c);
    ResponseModel responseModel;
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      Map map = apiResponse.response.data;
      String username = map["token"];
      // ignore: unused_local_variable
      String id = map["id"].toString();

      responseModel = ResponseModel(true, "Success");
      authRepo.saveUserToken(username);
    } else {
      _isLoading = false;

      String errorMessage;
      if (apiResponse.error is String) {
        errorMessage = apiResponse.error.toString();
      } else {
        errorMessage = apiResponse.error.errors[0].message;
      }
      // print(errorMessage);
      _loginErrorMessage = errorMessage;
      responseModel = ResponseModel(false, errorMessage);
    }
    _isLoading = false;
    notifyListeners();
    return responseModel;
  }
}
