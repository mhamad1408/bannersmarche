import 'package:flutter/material.dart';
import 'package:marche/data/response/api_response.dart';
import 'package:marche/model/info_data.dart';
import 'package:marche/repsetories/Info_data_repo.dart';

class InfoDataProvider extends ChangeNotifier {
  InfoDataRepo infoDataRepo;
  InfoDataProvider({@required this.infoDataRepo});
  List<InfoDataModel> _infoList;
  List<InfoDataModel> get infoList => _infoList;
  Future<void> getInfo(BuildContext context, bool reload) async {
    if (_infoList == null || reload) {
      ApiResponse apiResponse = await infoDataRepo.getinfo();
      if (apiResponse.response != null &&
          apiResponse.response.statusCode == 200) {
        _infoList = [];
        apiResponse.response.data['data'].forEach((x) {
          InfoDataModel infolModel = InfoDataModel.fromJson(x);
          _infoList.add(infolModel);
        });
      } else {
        print('error: ' + apiResponse.response.data);
        // //ApiChecker.checkApi(context, apiResponse);
      }
    }
    notifyListeners();
  }
}
