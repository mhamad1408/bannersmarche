import 'package:flutter/material.dart';
import 'package:marche/data/response/api_response.dart';
import 'package:marche/model/arrivals.dart';
import 'package:marche/repsetories/slides_repo.dart';

class SlidesProvider extends ChangeNotifier {
  SlidesRepo slidesRepo;

  SlidesProvider({@required this.slidesRepo});
  List<Slides> _slidesList;
  List<Slides> get slidesList => _slidesList;
  Future<void> getSlides(BuildContext context, bool reload) async {
    if (_slidesList == null || reload) {
      ApiResponse apiResponse = await slidesRepo.getslides();
      if (apiResponse.response != null &&
          apiResponse.response.statusCode == 200) {
        _slidesList = [];
        apiResponse.response.data['data'].forEach((x) {
          Slides slidesModel = Slides.fromJson(x);
          _slidesList.add(slidesModel);
        });
      } else {
        print('error: ' + apiResponse.response.data);
        // //ApiChecker.checkApi(context, apiResponse);
      }
    }
    notifyListeners();
  }
}
