import 'package:flutter/material.dart';
import 'package:marche/data/response/api_response.dart';
import 'package:marche/model/brands.dart';
import 'package:marche/repsetories/brands_repo.dart';

class BrandProvider extends ChangeNotifier {
  BrandRepo brandRepo;
  BrandProvider({@required this.brandRepo});

  List<BrandsModel> _brandList;
  List<BrandsModel> get brandList => _brandList;
  Future<void> getBrand(BuildContext context, bool reload) async {
    if (_brandList == null || reload) {
      ApiResponse apiResponse = await brandRepo.getbrand();

      if (apiResponse.response != null &&
          apiResponse.response.statusCode == 200) {
        _brandList = [];
        apiResponse.response.data['data'].forEach((x) {
          BrandsModel brandModel = BrandsModel.fromJson(x);
          _brandList.add(brandModel);
        });
      } else {
        print('error: ' + apiResponse.response.data);
        // //ApiChecker.checkApi(context, apiResponse);
      }
    }
    notifyListeners();
  }
}
