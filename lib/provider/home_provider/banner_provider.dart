import 'package:flutter/material.dart';
import 'package:marche/data/response/api_response.dart';
import 'package:marche/model/banners.dart';
import 'package:marche/repsetories/banner_repo.dart';

class BannerProvider extends ChangeNotifier {
  BannerRepo bannersRepo;
  BannerProvider({@required this.bannersRepo});

  List<Banners> _bannersList;
  List<Banners> get bannnersList => _bannersList;

  Future<void> getBanners(BuildContext context, bool reload) async {
    if (_bannersList == null || reload) {
      ApiResponse apiResponse = await bannersRepo.getbanners();

      if (apiResponse.response != null &&
          apiResponse.response.statusCode == 200) {
        _bannersList = [];
        apiResponse.response.data['data'].forEach((x) {
          Banners bannersModel = Banners.fromJson(x);
          _bannersList.add(bannersModel);
        });
      } else {
        print('error: ' + apiResponse.response.data);
        // //ApiChecker.checkApi(context, apiResponse);
      }
    }
    notifyListeners();
  }
}



//Map<String, dynamic>_banners = {};
// bool _error = false;
// String _errorMassage = '';
// Banners _BannersList;
// Banners get BannersList => _BannersList;

// Map<String , dynamic> get banners => _banners;
// bool get error  => _error;
// String get errorMassage => _errorMassage;

// Future<void> get getBanners async{
//   final response = await get(Uri.parse('https://lemarche.verozonetesting.com/api/getBanners')
//   );

//   if(response.statusCode ==200){
//     try {
//       _banners = jsonDecode(response.body);
//       _error = false;
//     }
//     catch(e)
//     {
//       _error = true;
//       _errorMassage = e.toString();
//       _banners = {};
//     }
//   }
//   else {
//     _error = true;
//     _errorMassage = 'You Are In Bad Days';
//     _banners = {};
//   }
//   notifyListeners();
// }


//  void initialValues(){
// //     _error = true;
// //     _errorMassage = '';
// //     _banners = {};
// //     notifyListeners();
// //  }


////////////////////futterbuilder
  //  List<Data>_banners ;
  //  Future<List<Data>> databanners;
  // Future<Banners> getBanners() async{
  // String url = "https://lemarche.verozonetesting.com/api/getBanners";
  // var response = await http.get(Uri.parse(url));
  // if (response.statusCode == 200) {
  // String body= utf8.decode(response.bodyBytes);
  // dynamic parse = json.decode(body);  
  // return Banners.fromJson(parse);
  // }
  // else{
  //   throw Exception('Failed to load banners');
  // }
  // }