import 'package:flutter/material.dart';
import 'package:marche/data/response/api_response.dart';
import 'package:marche/model/related_products.dart';
import 'package:marche/repsetories/related_products_repo.dart';

class RelatedProdcutsProvider extends ChangeNotifier {
  RelatedProductsRepo relatedproductsRepo;

  RelatedProdcutsProvider({@required this.relatedproductsRepo});
  List<RelatedProductsModel> _relatedprodcutsList;
  List<RelatedProductsModel> get relatedprodcutsList => _relatedprodcutsList;
  Future<void> getRelatedProducts(BuildContext context, bool reload) async {
    if (_relatedprodcutsList == null || reload) {
      ApiResponse apiResponse = await relatedproductsRepo.getrelatedproducts();
      if (apiResponse.response != null &&
          apiResponse.response.statusCode == 200) {
        _relatedprodcutsList = [];
        apiResponse.response.data['data'].forEach((x) {
          RelatedProductsModel relatedproductsModel =
              RelatedProductsModel.fromJson(x);
          _relatedprodcutsList.add(relatedproductsModel);
        });
      } else {
        print('error: ' + apiResponse.response.data);
        // //ApiChecker.checkApi(context, apiResponse);
      }
    }
    notifyListeners();
  }
}
