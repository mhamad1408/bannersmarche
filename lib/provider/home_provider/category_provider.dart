import 'package:flutter/material.dart';
import 'package:marche/data/response/api_response.dart';
import 'package:marche/model/category.dart';
import 'package:marche/repsetories/category_repo.dart';

class CategoryProvider extends ChangeNotifier {
  CategoryRepo categoryRepo;
  CategoryProvider({@required this.categoryRepo});
  List<CategoryModel> _categoryList;
  List<CategoryModel> get categorList => _categoryList;
  Future<void> getCategory(BuildContext context, bool reload) async {
    if (_categoryList == null || reload) {
      ApiResponse apiResponse = await categoryRepo.getcategory();

      if (apiResponse.response != null &&
          apiResponse.response.statusCode == 200) {
        _categoryList = [];
        apiResponse.response.data['data'].forEach((x) {
          CategoryModel categoryModel = CategoryModel.fromJson(x);
          _categoryList.add(categoryModel);
        });
      } else {
        print('error: ' + apiResponse.response.data);
        // //ApiChecker.checkApi(context, apiResponse);
      }
    }
    notifyListeners();
  }
}
