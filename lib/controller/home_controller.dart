import 'package:get/get.dart';
import 'package:marche/model/produts.dart';
import 'package:marche/servcies/itemservices.dart';

class HomePageController extends GetxController {
  ItemServices itemServices = ItemServices();
  List<ProductModel> items = [];
  List<ProductModel> cartItems = [];
  bool isLoading = true;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    loadDB();
  }

  loadDB() async {
    await itemServices.openDB();
    loadItems();
    getCardList();
  }

  getItem(int id) {
    return items.singleWhere((element) => element.productId == id);
  }

  bool isAlreadyInCart(id) {
    return cartItems.indexWhere((element) => element.categoryId == id) > -1;
  }

  getCardList() async {
    try {
      List list = await itemServices.getCartList();
      cartItems.clear();
      list.forEach((element) {
        cartItems.add(ProductModel.fromJson(element));
      });
      update();
    } catch (e) {
      print(e);
    }
  }

  loadItems() async {
    try {
      isLoading = true;
      update();

      List list = await itemServices.loadItems();
      list.forEach((element) {
        items.add(ProductModel.fromJson(element));
      });

      isLoading = false;
      update();
    } catch (e) {
      print(e);
    }
  }

  // setToFav(int id, bool flag) async {
  //   int index = items.indexWhere((element) => element.id == id);

  //   items[index].fav = flag;
  //   update();
  //   try {
  //     await itemServices.setItemAsFavourite(id, flag);
  //   } catch (e) {
  //     print(e);
  //   }
  // }

  Future addToCart(ProductModel item) async {
    isLoading = true;
    update();
    var result = await itemServices.addToCart(item);
    isLoading = false;
    update();
    return result;
  }

  removeFromCart(int productId) async {
    itemServices.removeFromCart(productId);
    int index =
        cartItems.indexWhere((element) => element.productId == productId);
    cartItems.removeAt(index);
    update();
  }
}
