import 'package:flutter/material.dart';
import 'package:marche/main.dart';
import 'package:marche/model/client.dart';
import 'package:marche/provider/home_provider/auth_provider.dart';
import 'package:marche/screen/constant/constant.dart';
import 'package:marche/screen/Sign_up/sign_up.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  GlobalKey<FormState> _formKeyLogin = GlobalKey<FormState>();
  ClientModel _clientModel = ClientModel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        scrollDirection: Axis.vertical,
        children: [
          SingleChildScrollView(
              child: Consumer<AuthProvider>(
            builder: (context, authProvider, child) => Form(
              key: _formKeyLogin,
              child: SafeArea(
                child: Stack(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.blue.shade600,
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 15, left: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          InkWell(
                            child: Icon(
                              Icons.arrow_back_sharp,
                              color: Colors.white,
                              size: 40,
                            ),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => MyHomePage()));
                            },
                          ),
                          const SizedBox(
                            width: 15,
                          ),
                          Text(
                            "Sign In",
                            style: TextStyle(color: Colors.white, fontSize: 25),
                          )
                        ],
                      ),
                    ),
                    ///////////////////////////
                    //TopSginup(),
                    Positioned(
                      top: MediaQuery.of(context).size.height * 0.10,
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.9,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: whiteshade,
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(45),
                              topRight: Radius.circular(45)),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 85,
                              width: MediaQuery.of(context).size.width * 0.8,
                              margin: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.09),
                              child: Image.asset(
                                "assets/images/logo png.png",
                              ),
                            ),
                            const SizedBox(
                              height: 12,
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 15.0, right: 15.0, top: 0, bottom: 15),
                              child: TextFormField(
                                validator: (value) {
                                  if (value.isEmpty) return ('Enter Username');
                                  return null;
                                },
                                onSaved: (result) {
                                  _clientModel.username = result;
                                },
                                keyboardType: TextInputType.visiblePassword,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: ('Uesername'),
                                    hintText: ('Enter valid a Username')),
                              ),
                            ),
                            //TextFormField(hintTexti: "Username"),
                            const SizedBox(
                              height: 15,
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 15.0, right: 15.0, top: 0, bottom: 15),
                              child: TextFormField(
                                obscureText: true,
                                validator: (value) {
                                  if (value.isEmpty) return ('Enter Password');
                                  return null;
                                },
                                onSaved: (result) {
                                  _clientModel.password = result;
                                },
                                keyboardType: TextInputType.visiblePassword,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: ('Password'),
                                    hintText: ('Enter valid a Password')),
                              ),
                            ),
                            // _TextFormFieldPassword(
                            //   //headerText: "Password",
                            //   hintTexti: "At least 8 Charecter",
                            // ),
                            //CheckerBox(),
                            const SizedBox(
                              height: 12,
                            ),
                            Container(
                              alignment: Alignment.center,
                              child: Text(
                                'Forget Password',
                                style: TextStyle(
                                  color: Colors.blue,
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            InkWell(
                              onTap: () {
                                print("Sign up click");
                              },
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                height:
                                    MediaQuery.of(context).size.height * 0.07,
                                margin:
                                    const EdgeInsets.only(left: 45, right: 45),
                                decoration: BoxDecoration(
                                  color: Colors.orange,
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(10),
                                  ),
                                ),
                                child: !authProvider.isLoading
                                    ? ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                            primary: Colors.orange),
                                        onPressed: () async {
                                          var isValid = _formKeyLogin
                                              .currentState
                                              .validate();
                                          if (isValid) {
                                            _formKeyLogin.currentState.save();
                                            authProvider
                                                .login(_clientModel)
                                                .then((status) async {
                                              print('login');
                                              print(status);
                                              if (status.isSuccess) {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            MyHomePage()));
                                              }
                                            }).onError(
                                                    (error, stackTrace) async {
                                              print('1errr');
                                            });
                                          }
                                        },
                                        child: Text(
                                          ('Sign in'),
                                          //locale: context.locale,
                                          style: TextStyle(fontSize: 20),
                                        ),
                                      )
                                    : Center(
                                        child: CircularProgressIndicator()),
                                // child: Center(
                                //   child: Text(
                                //     "Sign In",
                                //     style: TextStyle(
                                //       fontSize: 24,
                                //       fontWeight: FontWeight.w500,
                                //       color: whiteshade,
                                //     ),
                                //   ),
                                // ),
                              ),
                            ),
                            const SizedBox(
                              height: 12,
                            ),
                            Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(
                                //left: MediaQuery.of(context).size.width * 0.18,
                                top: MediaQuery.of(context).size.height * 0.02,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    'I Dont Have an account',
                                    style: TextStyle(
                                        color: grayshade.withOpacity(0.8),
                                        fontSize: 16),
                                  ),
                                  SizedBox(
                                    width: 2,
                                  ),
                                  InkWell(
                                    child: Text(
                                      'Sign Up',
                                      style: TextStyle(
                                          color: Colors.blue, fontSize: 16),
                                    ),
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => SingUp()));
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )),
        ],
      ),
    );
  }
}

// ignore: must_be_immutable
// class InputField extends StatelessWidget {
//   ///String headerText;
//   String hintTexti;
//   InputField({@required this.hintTexti});

//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         // Container(
//         //   margin: const EdgeInsets.only(
//         //     left: 50,
//         //     right: 50,
//         //     bottom: 10,
//         //   ),
//         //   child: Text(
//         //     headerText,
//         //     // style: const TextStyle(
//         //     //     color: Colors.green, fontSize: 14, fontWeight: FontWeight.w500),
//         //   ),
//         // ),
//         Container(
//             margin: const EdgeInsets.only(left: 35, right: 35),
//             decoration: BoxDecoration(
//               color: Colors.white,
//               border: Border.all(
//                 width: 1.4,
//                 color: Colors.lightBlue
//               ),
//               borderRadius: BorderRadius.circular(15),
//             ),
//             child: Padding(
//               padding: const EdgeInsets.symmetric(horizontal: 12.0),
//               child: TextField(
//                 decoration: InputDecoration(
//                   hintText: hintTexti,
//                   border: InputBorder.none,
//                 ),
//               ),
//             )
//             //IntrinsicHeight

//             ),
//       ],
//     );
//   }
// }
// ignore: must_be_immutable
// class InputFieldPassword extends StatefulWidget {
//   //String headerText;
//   String hintTexti;

//   InputFieldPassword(
//       { @required this.hintTexti});
//   @override
//   State<InputFieldPassword> createState() => _InputFieldPasswordState();
// }

// class _InputFieldPasswordState extends State<InputFieldPassword> {
//   bool _visible = true;

//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         // Container(
//         //   margin: const EdgeInsets.only(
//         //     left: 50,
//         //     right: 50,
//         //     bottom: 10,
//         //   ),
//         //   child: Text(
//         //     widget.headerText,
//         //     style: const TextStyle(
//         //         color: Colors.green, fontSize: 14, fontWeight: FontWeight.w500),
//         //   ),
//         // ),
//         Container(
//           margin: const EdgeInsets.only(left: 35, right: 35),
//           decoration: BoxDecoration(
//             color: Colors.white,
//             border: Border.all(
//               width: 1,
//               color: Colors.lightBlue
//             ),
//             borderRadius: BorderRadius.circular(15),
//           ),
//           child: Padding(
//             padding: const EdgeInsets.symmetric(horizontal: 12.0),
//             child: TextField(
//               obscureText: _visible,
//               decoration: InputDecoration(
//                   hintText: widget.hintTexti,
//                   border: InputBorder.none,
//                   suffixIcon: IconButton(
//                       icon: Icon(
//                           _visible ? Icons.visibility : Icons.visibility_off),
//                       onPressed: () {
//                         setState(() {
//                           _visible = !_visible;
//                         });
//                       })),
//             ),
//           ),
//         ),
//       ],
//     );
//   }
// }

class TopSginin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 15, left: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            child: Icon(
              Icons.arrow_back_sharp,
              color: Colors.white,
              size: 40,
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MyHomePage()));
            },
          ),
          const SizedBox(
            width: 15,
          ),
          Text(
            "Sign In",
            style: TextStyle(color: Colors.white, fontSize: 25),
          )
        ],
      ),
    );
  }
}
//////////////////////////////////////////////////
//body: ListView(
//   children: <Widget>[
//      Row(
//     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//     children: <Widget>[
//        InkWell(
//             child: Container(
//             height: 100.0,
//             width: 106.0,
//             //padding: EdgeInsets.all(10),
//             //margin: EdgeInsets.all(10),
//               alignment: Alignment.topCenter,
//             decoration: BoxDecoration(
//             borderRadius: BorderRadius.circular(10),
//             ),
//             child: ListTile(
//             title: Image.asset('assets/images/logo png.png',fit:BoxFit.cover,height: 60,width: 60,
//             ),
//             ),
//             ),
//            onTap: (){
//           },
//        ),
//         InkWell(
//           child: Container(
//           height: 100.0,
//           width: 106.0,
//           //padding: EdgeInsets.all(10),
//           //margin: EdgeInsets.all(10),
//               alignment: Alignment.topCenter,
//           decoration: BoxDecoration(
//           borderRadius: BorderRadius.circular(10),
//           ),
//           child: ListTile(
//           title:
//           IconButton(
//                      icon: Icon(
//                      Icons.close,
//                      color: Colors.black,
//                      ),
//                 onPressed: () {
//               // do something
//                Navigator.pop(context);
//             },
//           ),
//           ),
//           ),
//           onTap: (){
//           Navigator.pop(context);
//           },
//       ),
//     ],
//     ),
// Container(
// alignment: Alignment.topCenter,
// child: Image.asset('assets/images/logo png.png',fit:BoxFit.cover),
// ),
//  Container(
//  padding: EdgeInsets.only(bottom:40.0),
//  alignment: Alignment.topCenter,
//  child: RichText(
//  text: TextSpan(
//   text: 'Hala!',
//   style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black, fontSize: 30
//   ),
//    children: const <TextSpan>[
//     TextSpan(text: 'Welcome', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.blue, fontSize: 30
//     )),
//     TextSpan(text: 'back',
//     style:TextStyle(
//     fontWeight: FontWeight.bold, color: Colors.red,fontSize: 30
//     ),
//     ),
//   ],
// ),
// ),
// ),
//   Container(
//   child: Form(
//   key: _formKey,
//   child: Column(
//   mainAxisAlignment: MainAxisAlignment.center,
//   children: <Widget>[
//    Container(
//      alignment: Alignment.topLeft,
//      child: Text('Enter Your Email')
//    ),
//     _buildEmail(),
//     Container(
//     alignment: Alignment.topLeft,
//     child: Text('Password')
//     ),
//     _buildPassword(),
//     SizedBox(height: 40),
//     Text('Forgot your Password',
//     style: TextStyle(fontWeight: FontWeight.w500,fontSize:20),
//     ),
//      SizedBox(height: 10),
//     ElevatedButton(child: Text('Sign in',
//     ),
//     onPressed: (){
//     if (!_formKey.currentState.validate()) {
//     return;
//     }
//       _formKey.currentState.save();
//       print(_email);
//       print(_password);
//     },
//     ),
//     SizedBox(height: 5),
//       Row(
//    mainAxisAlignment: MainAxisAlignment.center,
//    children: <Widget>[
//    Text('New User ?'),

//    InkWell(
//           child: Text('Create Account',
//      style: TextStyle(
//      color: Colors.blue,
//      ),
//      ),
//      onTap: (){
//       Navigator.push(context,MaterialPageRoute(builder:(context)=>SingUp()));
//      },
//    )
//    ],
//    ),
//   ],
//   ),
//   ),
//   ),
//    ],
//    ),
