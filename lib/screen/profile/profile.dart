import 'package:flutter/material.dart';
import 'package:marche/screen/Sign_in/log.dart';
import 'package:marche/screen/Sign_up/sign_up.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/images/logo png.png',
              fit: BoxFit.cover,
              height: 100,
            ),
          ],
        ),
        centerTitle: true,
        elevation: 6,
        //  actions: <Widget>[
        //  IconButton(icon: Icon(Icons.search),color: Colors.red,onPressed: (){
        //  showSearch(context: context, delegate: SearchData());
        // }),
        //  ],
      ),
      body: ListView(
        children: <Widget>[
          Container(
            child: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(10),
                  child: Text(
                    ' Hala Nice to meet you',
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              InkWell(
                child: Container(
                  height: 100.0,
                  width: 106.0,
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.all(10),
                  alignment: Alignment.topCenter,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: ListTile(
                    title: CircleAvatar(
                      radius: 100,
                      backgroundImage: AssetImage(
                        'assets/images/per1.png',
                      ),
                      //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
                    ),
                    subtitle: Text(
                      'Sign In',
                      style:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => LoginScreen()));
                },
              ),
              InkWell(
                child: Container(
                  height: 100.0,
                  width: 106.0,
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.all(10),
                  alignment: Alignment.topCenter,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: ListTile(
                    title: CircleAvatar(
                      radius: 100,
                      backgroundImage: AssetImage(
                        'assets/images/per2.ico',
                      ),
                      //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
                    ),
                    subtitle: Text(
                      'Sign Up',
                      style:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SingUp()));
                },
              ),
              // Container(
              //        height: 50.0,
              //         width: 106.0,

              //         padding: EdgeInsets.all(10),
              //         margin: EdgeInsets.all(25),
              //       // alignment: Alignment.topCenter,
              //         decoration: BoxDecoration(
              //         color: Colors.blue,
              //         borderRadius: BorderRadius.circular(20),
              //         ),
              //         child: ListTile(
              //         title: Icon(Icons.person),
              //         subtitle: Text('Sign In'),
              //         ),
              //         ) ,
              // Container(
              // height: 50.0,
              // width: 106.0,
              // padding: EdgeInsets.all(10),
              // margin: EdgeInsets.all(25),
              // decoration: BoxDecoration(
              // borderRadius: BorderRadius.circular(20),
              // color: Colors.blue,
              // ),
              // child: ListTile(
              // title: Icon(Icons.person_add),
              // subtitle: Text('Sign Up'),
              // ),
              // ) ,
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Stack(
            children: <Widget>[
              Container(
                width: double.infinity,
                height: 30,
                color: Color.fromARGB(220, 220, 220, 220),
                child: Text(
                  'SETTINGS',
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
