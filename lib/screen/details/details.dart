import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:marche/controller/home_controller.dart';
import 'package:marche/data/response/config_model.dart';
import 'package:marche/model/cart.dart';
import 'package:marche/model/produts.dart';
import 'package:marche/model/related_products.dart';
import 'package:marche/provider/home_provider/cart_provider.dart';
// ignore: unused_import
import 'package:marche/provider/home_provider/products_provider.dart';
import 'package:marche/provider/home_provider/related_products_provider.dart';
import 'package:marche/screen/CheckoutScreen/CheckoutScreen.dart';
import 'package:provider/provider.dart';

class Details extends StatefulWidget {
  final ProductModel product;
  const Details({@required this.product});

  @override
  State<Details> createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  int active = 0;
  List<RelatedProductsModel> relatedproducts;
  //Prodcuts Start Data
  // Future<void> _produtsData(BuildContext context, bool reload) async {
  //   await Provider.of<ProductsProvider>(context, listen: false)
  //       .getProducts(context, reload, widget.subcaeegoryId);
  // }

  // Prodcuts End Data

  // Related Products Start Data
  Future<void> _relatedprodutsData(BuildContext context, bool reload) async {
    await Provider.of<RelatedProdcutsProvider>(context, listen: false)
        .getRelatedProducts(context, reload);
  }

  // Related Products End Data

  @override
  void initState() {
    // _produtsData(context, true);
    _relatedprodutsData(context, true);
    super.initState();
    // databanners = getBanners();
  }

  @override
  Widget build(BuildContext context) {
    CartModel _c =
        CartModel(widget.product.productPrice.toDouble(), 1, widget.product);
    return Scaffold(
      body: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          SizedBox(
            height: 8,
          ),
          Container(
            child: InkWell(
              child: Container(
                padding: EdgeInsets.only(right: 300),
                //height: 100.0,
                ///width: 106.0,
                //padding: EdgeInsets.all(10),
                //margin: EdgeInsets.all(10),
                //alignment: Alignment.topLeft,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: ListTile(
                  title: IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.black,
                    ),
                    onPressed: () {
                      // do something
                      Navigator.pop(context);
                    },
                  ),
                ),
              ),
            ),
          ),
          Container(
            alignment: Alignment.topCenter,
            child: InkWell(
              child: Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.all(10),
                alignment: Alignment.topCenter,
                width: 150,
                height: 220,
                // decoration: BoxDecoration(
                //   borderRadius: BorderRadius.all(Radius.zero),
                //   border: Border.all(
                //     color: Colors.black,
                //   ),
                // ),
                child: Column(
                  children: [
                    Container(
                      height: 100,
                      // width: 100,
                      child: Image.network(
                        BaseUrls.filesUrl +
                            widget.product.slides.first.productImageImg,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Text(
                      widget.product.productName,
                      overflow: TextOverflow.ellipsis,
                      style:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      widget.product.productPrice.toString(),
                      style: TextStyle(color: Colors.red),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    ElevatedButton(
                      child: Text('Add To Cart'),
                      onPressed: () {
                        Provider.of<CartProvider>(context, listen: false)
                            .addToCart(_c, null);
                        print(_c.product.productName);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => CheckoutPage()));
                      },
                    )
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),

          //Related Products
          Padding(
            padding: const EdgeInsets.only(top: 2.0),
            child: Container(
              height: 28,
              width: 15,
              alignment: Alignment.topCenter,
              color: Colors.blue,
              child: Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(
                  'Related Items',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
          Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.all(10),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Consumer<RelatedProdcutsProvider>(
                    builder: (context, relatedproductsProvider, child) {
                      relatedproducts =
                          relatedproductsProvider.relatedprodcutsList;
                      return relatedproducts != null &&
                              relatedproducts.length > 0
                          ? Row(
                              children: relatedproducts
                                  .map<Widget>((e) => InkWell(
                                        child: Container(
                                          padding: EdgeInsets.all(10),
                                          margin: EdgeInsets.all(10),
                                          alignment: Alignment.topCenter,
                                          width: 150,
                                          height: 190,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.all(Radius.zero),
                                            border: Border.all(
                                              color: Colors.black,
                                            ),
                                          ),
                                          child: Column(
                                            children: [
                                              Container(
                                                height: 90,
                                                child: Image.network(
                                                  BaseUrls.filesUrl +
                                                      e.slides.first
                                                          .productImageImg,
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                              Text(
                                                e.productName,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              SizedBox(
                                                height: 5,
                                              ),
                                              Text(
                                                e.productPrice.toString(),
                                                style: TextStyle(
                                                    color: Colors.red),
                                              ),
                                              SizedBox(
                                                height: 5,
                                              ),
                                              Icon(Icons.add_shopping_cart,
                                                  color: Colors.blue),
                                            ],
                                          ),
                                        ),
                                        // onTap: () {
                                        //   Navigator.push(
                                        //       context,
                                        //       MaterialPageRoute(
                                        //           builder: (context) =>
                                        //               Details()));
                                        // },
                                      ))
                                  .toList())
                          : Center(child: CircularProgressIndicator());
                    },
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
