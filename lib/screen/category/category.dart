import 'package:flutter/material.dart';
import 'package:marche/model/category.dart';
import 'package:marche/provider/home_provider/category_provider.dart';
//import 'package:marche/screen/appliances/appliances.dart';
import 'package:marche/screen/bakery/bakery.dart';
import 'package:marche/screen/home/home.dart';
import 'package:marche/screen/tabs.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class Category extends StatefulWidget {
  @override
  State<Category> createState() => _CategoryState();
}

class _CategoryState extends State<Category> {
  //Category Start Data
  Future<void> _loadCaategory(BuildContext context, bool reload) async {
    await Provider.of<CategoryProvider>(context, listen: false)
        .getCategory(context, reload);
  }

  //Category End Data

  List<CategoryModel> category;

  int selectedindex = 0;
  int index = 0;
  @override
  void initState() {
    _loadCaategory(context, true);
    super.initState();
    // databanners = getBanners();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/images/logo png.png',
              fit: BoxFit.cover,
              height: 100,
            ),
          ],
        ),
        centerTitle: true,
        elevation: 6,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.search),
              color: Colors.red,
              onPressed: () {
                showSearch(context: context, delegate: SearchData());
              }),
        ],
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                //width: 200,
                child: Consumer<CategoryProvider>(
                    builder: (context, categoryProvider, child) {
                  category = categoryProvider.categorList;
                  return category != null && category.length > 0
                      ? VerticalTabs(
                          tabsWidth: 100,
                          tabs: category
                              .map<Tab>((e) => Tab(
                                      child: Container(
                                    // height: 65,
                                    width: 0.0,
                                    margin:
                                        EdgeInsets.only(bottom: 1.0, left: 1.0),
                                    //alignment: Alignment.topCenter,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: ListTile(
                                      // title: CircleAvatar(
                                      //   radius: 100,
                                      //   backgroundImage: AssetImage(
                                      //     'assets/images/m1.png',
                                      //   ),
                                      //   //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
                                      // ),
                                      subtitle: Text(
                                        e.categoryName,
                                        style: TextStyle(
                                            fontSize: 11,
                                            fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    //child: Text(e.categoryName),
                                  )))
                              .toList(),
                          contents: category
                              .map<Widget>((e) => Container(
                                    child: Bakery(categoryModel: e),
                                  ))
                              .toList())
                      : CircularProgressIndicator();
                }),
                // child: VerticalTabs(
                //   tabsWidth: 90,
                //   tabs: <Tab>[
                //     Tab(
                //       child: Text('Bakery'),
                //     ),
                //     Tab(
                //       child: Text('First Aid'),
                //     ),
                //     Tab(
                //       child: Container(
                //         margin: EdgeInsets.only(bottom: 1),
                //         child: Row(
                //           children: <Widget>[
                //             // Icon(Icons.favorite),
                //             //SizedBox(width: 25),
                //             Text('Deli')
                //           ],
                //         ),
                //       ),
                //     ),
                //     Tab(child: Text('Dairy & Eggs')),
                //     Tab(child: Text('Cans & Jars') //Dairy & Eggs
                //         ),
                //     Tab(child: Text('Pantry')), //Cans & Jars
                //     Tab(
                //         child: Text(
                //       'Healthy Products',
                //       //overflow: TextOverflow.ellipsis,
                //     )),
                //     Tab(
                //         child: Text(
                //       'Breakfast Food',
                //       //overflow: TextOverflow.ellipsis,
                //     )),
                //     Tab(
                //         child: Text(
                //       'Chocolate & Snacks',
                //       //overflow: TextOverflow.ellipsis,
                //     )),
                //     Tab(
                //         child: Text(
                //       'Frozen & Chilled',
                //       //overflow: TextOverflow.ellipsis,
                //     )),
                //     Tab(
                //         child: Text(
                //       'Beverages & Juices',
                //       //overflow: TextOverflow.ellipsis,
                //     )),
                //     Tab(
                //         child: Text(
                //       'Wine & Alcohol',
                //       //overflow: TextOverflow.ellipsis,
                //     )),
                //     Tab(child: Text('Tobacco')),
                //     Tab(
                //         child: Text(
                //       'Kitchen & Dining',
                //       //overflow: TextOverflow.ellipsis,
                //     )),
                //     Tab(
                //         child: Text(
                //       'Beauty Boutique',
                //       //overflow: TextOverflow.ellipsis,
                //     )),
                //     Tab(child: Text('Hair Care')),
                //     Tab(
                //         child: Text(
                //       'Personal Care',
                //       //overflow: TextOverflow.ellipsis,
                //     )),
                //     Tab(
                //         child: Text(
                //       'Cleaning & Home Care',
                //       //overflow: TextOverflow.ellipsis,
                //     )),
                //     Tab(child: Text('Household')),
                //     Tab(child: Text('Electronics')),
                //     Tab(child: Text('Automotive')),
                //     Tab(child: Text('Kids')),
                //   ],
                //   contents: <Widget>[
                //     Container(
                //       child: Bakery(),
                //     ),
                //     Container(
                //       child: FirstAid(),
                //     ),
                //     Container(
                //       child: Deli(),
                //     ),
                //     Container(),
                //     Container(),
                //     Container(),
                //     Container(),
                //   ],
                // ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
