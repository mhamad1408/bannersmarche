import 'package:flutter/material.dart';
import 'package:marche/screen/categories/categories.dart';
import 'package:marche/screen/category/category.dart';

class Brands extends StatefulWidget {
  @override
  _BrandsState createState() => _BrandsState();
}

class _BrandsState extends State<Brands> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Brands'),
      ),
      //drawer: MyDrawer(),
      body: GridView(
        gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        children: <Widget>[
          Container(
            child: InkWell(
              child: Card(
                child: Column(
                  children: <Widget>[
                    Expanded(
                        child: Image.asset(
                      'assets/images/w16.png',
                      fit: BoxFit.cover,
                    )),
                    Expanded(
                      child: Text(
                        'Samsung',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ],
                ),
              ),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Category()));
              },
            ),
          ),
          Container(
            child: Card(
              child: Column(
                children: <Widget>[
                  Expanded(
                      child: Image.asset(
                    'assets/images/w13.png',
                    fit: BoxFit.cover,
                  )),
                  Expanded(
                    child: Text(
                      'Apple',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            child: Card(
              child: Column(
                children: <Widget>[
                  Expanded(
                      child: Image.asset(
                    'assets/images/w14.jpg',
                    fit: BoxFit.cover,
                  )),
                  Expanded(
                    child: Text(
                      'Lenovo',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
