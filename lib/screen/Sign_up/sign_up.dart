import 'package:flutter/material.dart';
import 'package:marche/main.dart';
import 'package:marche/model/client.dart';
import 'package:marche/provider/home_provider/auth_provider.dart';
import 'package:marche/screen/Sign_in/log.dart';
import 'package:marche/screen/constant/constant.dart';
import 'package:provider/provider.dart';

class SingUp extends StatefulWidget {
  @override
  _SingUpState createState() => _SingUpState();
}

class _SingUpState extends State<SingUp> {
  GlobalKey<FormState> _formKeyLogin = GlobalKey<FormState>();
  ClientModel _clientModel = ClientModel();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        scrollDirection: Axis.vertical,
        children: [
          SingleChildScrollView(
              child: Consumer<AuthProvider>(
            builder: (context, authProvider, child) => Form(
              key: _formKeyLogin,
              child: SafeArea(
                child: Stack(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.blue.shade600,
                    ),
                    TopSginup(),
                    Positioned(
                      top: MediaQuery.of(context).size.height * 0.10,
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.9,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: whiteshade,
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(45),
                              topRight: Radius.circular(45)),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 85,
                              width: MediaQuery.of(context).size.width * 0.8,
                              margin: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.09),
                              child: Image.asset(
                                "assets/images/logo png.png",
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 15.0, right: 15.0, top: 0, bottom: 15),
                              child: TextFormField(
                                validator: (value) {
                                  if (value.isEmpty) return ('Enter Name');
                                  return null;
                                },
                                onSaved: (result) {
                                  _clientModel.name = result;
                                },
                                keyboardType: TextInputType.visiblePassword,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: ('Name'),
                                    hintText: ('Enter valid a name')),
                              ),
                            ),
                            // TextFormField(
                            //   hintTexti: "FullName",
                            // ),
                            // const SizedBox(
                            //   height: 2,
                            // ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 15.0, right: 15.0, top: 0, bottom: 15),
                              child: TextFormField(
                                validator: (value) {
                                  if (value.isEmpty)
                                    return ('Enter Your Mobile');
                                  return null;
                                },
                                onSaved: (result) {
                                  _clientModel.phone = result;
                                },
                                keyboardType: TextInputType.visiblePassword,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: ('Mobile'),
                                    hintText: ('Enter Your Mobile Number')),
                              ),
                            ),
                            //TextFormField(hintTexti: "Mobile"),
                            // const SizedBox(
                            //   height: 2,
                            // ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 15.0, right: 15.0, top: 0, bottom: 15),
                              child: TextFormField(
                                validator: (value) {
                                  if (value.isEmpty) return ('Enter Email');
                                  return null;
                                },
                                onSaved: (result) {
                                  _clientModel.email = result;
                                },
                                keyboardType: TextInputType.visiblePassword,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: ('Email'),
                                    hintText: ('Example@gmail.com')),
                              ),
                            ),
                            //TextFormField(hintTexti: "dion@example.com"),
                            // const SizedBox(
                            //   height: 2,
                            // ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 15.0, right: 15.0, top: 0, bottom: 15),
                              child: TextFormField(
                                validator: (value) {
                                  if (value.isEmpty) return ('Enter Username');
                                  return null;
                                },
                                onSaved: (result) {
                                  _clientModel.username = result;
                                },
                                keyboardType: TextInputType.visiblePassword,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: ('Uesername'),
                                    hintText: ('Enter valid a Username')),
                              ),
                            ),
                            //TextFormField(hintTexti: "Username"),
                            const SizedBox(
                              height: 2,
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 15.0, right: 15.0, top: 0, bottom: 15),
                              child: TextFormField(
                                obscureText: true,
                                validator: (value) {
                                  if (value.isEmpty) return ('Enter Password');
                                  return null;
                                },
                                onSaved: (result) {
                                  _clientModel.password = result;
                                },
                                keyboardType: TextInputType.visiblePassword,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: ('Password'),
                                    hintText: ('Enter valid a Password')),
                              ),
                            ),
                            // _TextFormFieldPassword(
                            //   //headerText: "Password",
                            //   hintTexti: "At least 8 Charecter",
                            // ),
                            //CheckerBox(),
                            const SizedBox(
                              height: 2,
                            ),

                            Container(
                                width: MediaQuery.of(context).size.width,
                                height:
                                    MediaQuery.of(context).size.height * 0.07,
                                margin:
                                    const EdgeInsets.only(left: 45, right: 45),
                                decoration: BoxDecoration(
                                  color: Colors.orange,
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(10),
                                  ),
                                ),
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      primary: Colors.orange),
                                  onPressed: () async {
                                    var isValid =
                                        _formKeyLogin.currentState.validate();
                                    if (isValid) {
                                      _formKeyLogin.currentState.save();
                                      authProvider
                                          .register(_clientModel)
                                          .then((status) async {
                                        if (status.isSuccess) {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      MyHomePage()));
                                        }
                                      }).onError((error, stackTrace) async {});
                                    }
                                  },
                                  child: Text(
                                    ('Sign up'),
                                    style: TextStyle(
                                      fontSize: 20,
                                    ),
                                  ),
                                )
                                // child: Padding(
                                //   padding: const EdgeInsets.symmetric(
                                //       horizontal: 15, vertical: 15),
                                //   child: Container(
                                //     height: 50,
                                //     width: MediaQuery.of(context).size.width,
                                //     decoration: BoxDecoration(
                                //         color: Colors.blue,
                                //         borderRadius: BorderRadius.circular(20)),
                                //     // child: ElevatedButton(
                                //     //   // onPressed: () async {
                                //     //   //   var isValid =
                                //     //   //       _formKeyLogin.currentState.validate();
                                //     //   //   if (isValid) {
                                //     //   //     _formKeyLogin.currentState.save();
                                //     //   //     authProvider
                                //     //   //         .register(_clientModel)
                                //     //   //         .then((status) async {
                                //     //   //       if (status.isSuccess) {
                                //     //   //         Navigator.push(
                                //     //   //             context,
                                //     //   //             MaterialPageRoute(
                                //     //   //                 builder: (context) =>
                                //     //   //                     Home()));
                                //     //   //       }
                                //     //   //     }).onError(
                                //     //   //             (error, stackTrace) async {});
                                //     //   //   }
                                //     //   // },
                                //     //   // child: Text(
                                //     //   //   "Sign up",
                                //     //   //   style: TextStyle(
                                //     //   //     fontSize: 24,
                                //     //   //     fontWeight: FontWeight.w500,
                                //     //   //     color: whiteshade,
                                //     //   //   ),
                                //     //   // ),
                                //     // ),
                                //   ),
                                // ),
                                ),
                            Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(
                                //left: MediaQuery.of(context).size.width * 0.18,
                                top: MediaQuery.of(context).size.height * 0.02,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    'I already Have an account',
                                    style: TextStyle(
                                        color: grayshade.withOpacity(0.8),
                                        fontSize: 16),
                                  ),
                                  SizedBox(
                                    width: 2,
                                  ),
                                  InkWell(
                                    child: Text(
                                      'Sign In',
                                      style: TextStyle(
                                          color: Colors.blue, fontSize: 16),
                                    ),
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  LoginScreen()));
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )),
        ],
      ),
    );
  }
}

// // ignore: must_be_immutable
// class TextFormField extends StatelessWidget {
//   //String headerText;
//   String hintTexti;
//   TextFormField({@required this.hintTexti});
//   @override
//   Widget build(BuildContext context) {
//     // String validator;
//     //  validator: (value) {
//     //           if (value == null || value.isEmpty) {
//     //             return 'Please enter some text';
//     //           }
//     //           return null;
//     //   };
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         // Container(
//         //   margin: const EdgeInsets.only(
//         //     left: 45,
//         //     right: 45,
//         //     //top: 20,
//         //     //bottom: 10,
//         //   ),
//         //   // child: Text(
//         //   //   headerText,
//         //   //   style: const TextStyle(
//         //   //       color: Colors.green, fontSize: 11, fontWeight: FontWeight.w500),
//         //   // ),
//         // ),

//         Container(
//             margin: const EdgeInsets.only(left: 40, right: 40),
//             decoration: BoxDecoration(
//               color: Colors.white,
//               //color: grayshade.withOpacity(0.5),
//               border: Border.all(width: 1.4, color: Colors.lightBlue),
//               borderRadius: BorderRadius.circular(15),
//             ),
//             child: Padding(
//               padding: const EdgeInsets.symmetric(horizontal: 12.0),
//               child: TextField(
//                 decoration: InputDecoration(
//                   hintText: hintTexti,
//                   border: InputBorder.none,
//                 ),
//               ),
//             )
//             //IntrinsicHeight

//             ),
//       ],
//     );
//   }
// }

// // ignore: must_be_immutable
// class _TextFormFieldPassword extends StatefulWidget {
//   //String headerText;
//   String hintTexti;
//   _TextFormFieldPassword({@required this.hintTexti});
//   @override
//   __TextFormFieldPasswordState createState() => __TextFormFieldPasswordState();
// }

// class __TextFormFieldPasswordState extends State<_TextFormFieldPassword> {
//   @override
//   Widget build(BuildContext context) {
//     bool _visible = true;
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         // Container(
//         //   margin: const EdgeInsets.only(
//         //     left: 45,
//         //     right: 45,
//         //     //bottom: 10,
//         //   ),
//         //   child: Text(
//         //     widget.headerText,
//         //     style: const TextStyle(
//         //         color: Colors.green, fontSize: 11, fontWeight: FontWeight.w500),
//         //   ),
//         // ),
//         Container(
//           margin: const EdgeInsets.only(left: 40, right: 40),
//           decoration: BoxDecoration(
//             color: Colors.white,
//             //color: grayshade.withOpacity(0.5),
//             border: Border.all(width: 1.4, color: Colors.lightBlue),
//             borderRadius: BorderRadius.circular(15),
//           ),
//           child: Padding(
//             padding: const EdgeInsets.symmetric(horizontal: 7.0),
//             child: TextField(
//               obscureText: _visible,
//               decoration: InputDecoration(
//                   hintText: widget.hintTexti,
//                   border: InputBorder.none,
//                   suffixIcon: IconButton(
//                       icon: Icon(
//                           _visible ? Icons.visibility : Icons.visibility_off),
//                       onPressed: () {
//                         setState(() {
//                           _visible = !_visible;
//                         });
//                       })),
//             ),
//           ),
//         ),
//       ],
//     );
//   }
// }

class TopSginup extends StatelessWidget {
  const TopSginup({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 15, left: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            child: Icon(
              Icons.arrow_back_sharp,
              color: whiteshade,
              size: 40,
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginScreen()));
            },
          ),
          const SizedBox(
            width: 15,
          ),
          Text(
            "Sign up",
            style: TextStyle(color: whiteshade, fontSize: 25),
          )
        ],
      ),
    );
  }
}


/// here is from
    //  Form(
    //         //key: _formKey,
    //         child: SafeArea(
    //           child: Stack(
    //             children: <Widget>[
    //               Container(
    //                 height: MediaQuery.of(context).size.height,
    //                 width: MediaQuery.of(context).size.width,
    //                 color: Colors.blue.shade600,
    //               ),
    //               TopSginup(),
    //               Positioned(
    //                 top: MediaQuery.of(context).size.height * 0.10,
    //                 child: Container(
    //                   height: MediaQuery.of(context).size.height * 0.9,
    //                   width: MediaQuery.of(context).size.width,
    //                   decoration: BoxDecoration(
    //                     color: whiteshade,
    //                     borderRadius: const BorderRadius.only(
    //                         topLeft: Radius.circular(45),
    //                         topRight: Radius.circular(45)),
    //                   ),
    //                   child: Column(
    //                     crossAxisAlignment: CrossAxisAlignment.start,
    //                     children: <Widget>[
    //                       Container(
    //                         height: 85,
    //                         width: MediaQuery.of(context).size.width * 0.8,
    //                         margin: EdgeInsets.only(
    //                             left: MediaQuery.of(context).size.width * 0.09),
    //                         child: Image.asset(
    //                           "assets/images/logo png.png",
    //                         ),
    //                       ),
    //                       TextFormField(
    //                         hintTexti: "FullName",
    //                       ),
    //                       const SizedBox(
    //                         height: 12,
    //                       ),
    //                       TextFormField(hintTexti: "Mobile"),
    //                       const SizedBox(
    //                         height: 12,
    //                       ),
    //                       TextFormField(hintTexti: "dion@example.com"),
    //                       const SizedBox(
    //                         height: 12,
    //                       ),
    //                       TextFormField(hintTexti: "Username"),
    //                       const SizedBox(
    //                         height: 12,
    //                       ),
    //                       _TextFormFieldPassword(
    //                         //headerText: "Password",
    //                         hintTexti: "At least 8 Charecter",
    //                       ),
    //                       //CheckerBox(),
    //                       const SizedBox(
    //                         height: 12,
    //                       ),
    //                       InkWell(
    //                         onTap: () {
    //                           print("Sign up click");
    //                         },
    //                         child: Container(
    //                           width: MediaQuery.of(context).size.width,
    //                           height: MediaQuery.of(context).size.height * 0.07,
    //                           margin:
    //                               const EdgeInsets.only(left: 45, right: 45),
    //                           decoration: BoxDecoration(
    //                             color: Colors.orange,
    //                             borderRadius: const BorderRadius.all(
    //                               Radius.circular(10),
    //                             ),
    //                           ),
    //                           child: Center(
    //                             child: Text(
    //                               "Sign up",
    //                               style: TextStyle(
    //                                 fontSize: 24,
    //                                 fontWeight: FontWeight.w500,
    //                                 color: whiteshade,
    //                               ),
    //                             ),
    //                           ),
    //                         ),
    //                       ),
    //                       Container(
    //                         alignment: Alignment.center,
    //                         margin: EdgeInsets.only(
    //                           //left: MediaQuery.of(context).size.width * 0.18,
    //                           top: MediaQuery.of(context).size.height * 0.02,
    //                         ),
    //                         child: Row(
    //                           mainAxisAlignment: MainAxisAlignment.center,
    //                           children: <Widget>[
    //                             Text(
    //                               'I already Have an account',
    //                               style: TextStyle(
    //                                   color: grayshade.withOpacity(0.8),
    //                                   fontSize: 16),
    //                             ),
    //                             SizedBox(
    //                               width: 6,
    //                             ),
    //                             InkWell(
    //                               child: Text(
    //                                 'Sign In',
    //                                 style: TextStyle(
    //                                     color: Colors.blue, fontSize: 16),
    //                               ),
    //                               onTap: () {
    //                                 Navigator.push(
    //                                     context,
    //                                     MaterialPageRoute(
    //                                         builder: (context) =>
    //                                             LoginScreen()));
    //                               },
    //                             ),
    //                           ],
    //                         ),
    //                       ),
    //                     ],
    //                   ),
    //                 ),
    //               ),
    //             ],
    //           ),
    //         ),
    //       ),