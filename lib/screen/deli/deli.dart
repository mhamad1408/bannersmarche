import 'package:flutter/material.dart';

class Deli extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
  body: ListView(
  children: <Widget>[
   SizedBox(
  height: 10,
  ),
  // Container For 2 image 
  Container(
    height: 150,
    //padding: EdgeInsets.all(8),
    margin: EdgeInsets.all(8),
  child: Image.asset('assets/images/ba.jpg',fit: BoxFit.cover),
  ),
  //  Container(
  //    height: 150,
  //    padding: EdgeInsets.all(8),
  //    margin: EdgeInsets.all(8),
  // child: Image.asset('assets/images/gh.jpg',fit: BoxFit.cover),
  // ),
  Container(
  padding: EdgeInsets.only(left:10),
   //padding: EdgeInsets.all(6),
   //margin: EdgeInsets.all(10),
  //alignment: Alignment.topRight,
  child: Text('Shop by Category',
  style: TextStyle(
  color: Colors.blueAccent,
  fontSize: 16,
  //fontWeight: FontWeight.bold
  ),
  ),
  ),
  //End Container for 2 images
      GridView.count(
      crossAxisCount: 2,
      primary: false,
      padding: const EdgeInsets.all(25),
      crossAxisSpacing: 6,
      mainAxisSpacing: 1,
      physics: NeverScrollableScrollPhysics(), // to disable GridView's scrolling
      shrinkWrap: true, // You won't see infinite size error
      children: <Widget>[
        Container(
          height: 250,
          //width: 200,
          //color: Colors.green,
          child: Stack(
          children: <Widget>[
          Container(
          height: 70,
          decoration: BoxDecoration(
          border: Border.all(),
          ),
          child: Image.asset('assets/images/ch1.jpg',fit: BoxFit.cover,width: 120,
          ),
          ),
           Container(
           alignment: Alignment.bottomCenter,
          height: 90,
          child: Text('Cheese',
          style: TextStyle(
          color: Colors.black,
          fontSize: 11,
          //fontWeight: FontWeight.bold,
          ),
          ),
          ),
          ],
          ),
        ),
        Container(
          height: 250,
          //color: Colors.blue,
           child: Stack(
          children: <Widget>[
          Container(
          height: 70,
          decoration: BoxDecoration(
          border: Border.all(),
          ),
          child: Image.asset('assets/images/dl.jpg',fit: BoxFit.cover,width: 120,
          ),
          ),
          Container(
           alignment: Alignment.bottomCenter,
          height: 90,
          child: Text('Deli Meat',
          style: TextStyle(
          color: Colors.black,
          fontSize: 11,
         // fontWeight: FontWeight.bold,
          ),
          ),
          ),
          ],
          ),
        ),
           Container(
          height: 250,
          //color: Colors.blue,
           child: Stack(
          children: <Widget>[
          Container(
          height: 70,
          decoration: BoxDecoration(
          border: Border.all(),
          ),
          child: Image.asset('assets/images/hal.png',fit: BoxFit.cover,width: 120,
          ),
          ),
          Container(
           alignment: Alignment.bottomCenter,
          height: 90,
          child: Text('Halawa',
          style: TextStyle(
          color: Colors.black,
          fontSize: 11,
         // fontWeight: FontWeight.bold,
          ),
          ),
          ),
          ],
          ),
        ),
        //   Container(
        //   height: 250,
        //   //color: Colors.red,
        //   padding: EdgeInsets.only(bottom: 30),
        //     child: Stack(
        //   children: <Widget>[
        //   Container(
        //   height: 70,
        //   decoration: BoxDecoration(
        //   border: Border.all(),
        //   ),
        //   child: Image.asset('assets/images/hal.png',fit: BoxFit.cover,width: 120,
        //   ),
        //   ),
        //    Container(
        //    alignment: Alignment.bottomCenter,
        //   height: 90,
        //   child: Text('Halawa',
        //   style: TextStyle(
        //   color: Colors.black,
        //   fontSize: 11,
        //   //fontWeight: FontWeight.bold,
        //   ),
        //   ),
        //   ),
        //   ],
        //   ),
        // ),
          Container(
          height: 250,
          //color: Colors.blue,
            child: Stack(
          children: <Widget>[
          Container(
          height: 70,
          decoration: BoxDecoration(
          border: Border.all(),
          ),
          child: Image.asset('assets/images/cre.jpg',fit: BoxFit.cover,width: 120,
          ),
          ),
           Container(
           alignment: Alignment.bottomCenter,
          height: 90,
          child: Text('Cream Cheese',
          style: TextStyle(
          color: Colors.black,
           fontSize: 11,
          //fontWeight: FontWeight.bold,
          ),
          ),
          ),
          ],
          ),
        ),
          Container(
          height: 250,
          //color: Colors.blue,
            child: Stack(
          children: <Widget>[
          Container(
          height: 70,
          decoration: BoxDecoration(
          border: Border.all(),
          ),
          child: Image.asset('assets/images/oli.png',fit: BoxFit.cover,width: 120,
          ),
          ),
           Container(
           alignment: Alignment.bottomCenter,
          height: 90,
          child: Text('Olives',
          style: TextStyle(
          color: Colors.black,
          fontSize: 11,
          //fontWeight: FontWeight.bold,
          ),
          ),
          ),
          ],
          ),
        ),
          Container(
          height: 250,
          //color: Colors.blue,
            child: Stack(
          children: <Widget>[
          Container(
          height: 70,
          decoration: BoxDecoration(
          border: Border.all(),
          ),
          child: Image.asset('assets/images/dlg.jpg',fit: BoxFit.cover,width: 120,),
          ),
           Container(
           alignment: Alignment.bottomCenter,
          height: 90,
          child: Text('Deli Gourmet',
          style: TextStyle(
          color: Colors.black,
          fontSize: 11,
          //fontWeight: FontWeight.bold,
          ),
          ),
          ),
          ],
          ),
        ),
          Container(
          height: 250,
          //color: Colors.blue,
            child: Stack(
          children: <Widget>[
          Container(
          height: 70,
          decoration: BoxDecoration(
          border: Border.all(),
          ),
          child: Image.asset('assets/images/sna.jpg',fit: BoxFit.cover,width: 120,
          ),
          ),
           Container(
           alignment: Alignment.bottomCenter,
          height: 90,
          child: Text('Snacking Ch',
          style: TextStyle(
          color: Colors.black,
          fontSize: 11,
          //fontWeight: FontWeight.bold,
          ),
          ),
          ),
          ],
          ),
        ),
            Container(
          height: 250,
          //color: Colors.blue,
            child: Stack(
          children: <Widget>[
          Container(
          height: 70,
          decoration: BoxDecoration(
          border: Border.all(),
          ),
          child: Image.asset('assets/images/gra.jpg',fit: BoxFit.cover,width: 120,
          ),
          ),
           Container(
           alignment: Alignment.bottomCenter,
          height: 90,
          child: Text('Sliced Cheese',
          style: TextStyle(
          color: Colors.black,
          fontSize: 11,
          //fontWeight: FontWeight.bold,
          ),
          ),
          ),
          ],
          ),
        ),
      ],
    ),
  ],
  ),
  );
}