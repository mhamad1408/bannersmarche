// import 'package:carousel_pro/carousel_pro.dart';
// import 'package:flutter/material.dart';
// import 'package:marche/screen/apple/apple.dart';
// import 'package:marche/screen/details/details.dart';
// import 'package:marche/screen/home/home.dart';

// class Television extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Row(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             Image.asset(
//               'assets/images/logo png.png',
//               fit: BoxFit.cover,
//               height: 100,
//             ),
//           ],
//         ),
//         centerTitle: true,
//         elevation: 6,
//         actions: <Widget>[
//           IconButton(
//               icon: Icon(Icons.search),
//               color: Colors.red,
//               onPressed: () {
//                 showSearch(context: context, delegate: SearchData());
//               }),
//         ],
//       ),
//       body: SafeArea(
//         // ignore: missing_return
//         child: LayoutBuilder(builder: (context, constraints) {
//           if (constraints.maxWidth < 760) {
//             return ListView(
//               scrollDirection: Axis.vertical,
//               children: <Widget>[
//                 SizedBox(
//                   height: 10,
//                 ),
//                 // Container(
//                 // color: Colors.teal,
//                 // child: Center(
//                 // child: Text('Television',
//                 //  style: TextStyle(
//                 //   fontSize: 20,
//                 //   color: Colors.white,
//                 //   fontWeight: FontWeight.bold,
//                 //   ),
//                 // ),
//                 // ),
//                 // ),
//                 // Begin Carousel
//                 Container(
//                   //padding: EdgeInsets.all(10),
//                   height: 200.0,
//                   width: double.infinity,
//                   child: Carousel(
//                     images: [
//                       Image.asset(
//                         'assets/images/sm1.jpeg',
//                         fit: BoxFit.cover,
//                       ),
//                       Image.asset(
//                         'assets/images/sm2.jpg',
//                         fit: BoxFit.cover,
//                       ),
//                       Image.asset(
//                         'assets/images/sm3.jpg',
//                         fit: BoxFit.cover,
//                       ),
//                       Image.asset(
//                         'assets/images/sm4.jpg',
//                         fit: BoxFit.cover,
//                       ),
//                       Image.asset(
//                         'assets/images/sm5.jpg',
//                         fit: BoxFit.cover,
//                       ),
//                       Image.asset(
//                         'assets/images/sm6.jpg',
//                         fit: BoxFit.cover,
//                       ),
//                     ],

//                     dotSpacing: 10,
//                     dotSize: 1.5,
//                     //dotIncreaseSize :0.5,
//                     dotColor: Colors.white,
//                     //dotBgColor: Colors.blue.withOpacity(0.5),
//                     //overlayShadow: true,
//                     //overlayShadowColors: Colors.blue,
//                   ),
//                 ),
//                 // End Carousel
// // Beign Text
//                 Container(
//                   height: 130.0,
//                   width: double.infinity,
//                   child: SingleChildScrollView(
//                     scrollDirection: Axis.horizontal,
//                     child: Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       children: <Widget>[
//                         Container(
//                           height: 85.0,
//                           width: 100.0,
//                           //padding: EdgeInsets.all(10),
//                           margin: EdgeInsets.only(bottom: 20.0, left: 20.0),
//                           alignment: Alignment.topCenter,
//                           decoration: BoxDecoration(
//                             borderRadius: BorderRadius.circular(10),
//                           ),
//                           child: Column(
//                             children: <Widget>[
//                               Container(
//                                 height: 50.0,
//                                 width: 86.0,
//                                 child: ListTile(
//                                   title: CircleAvatar(
//                                     radius: 100,
//                                     backgroundImage: AssetImage(
//                                       'assets/images/h5.jpg',
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                               SizedBox(
//                                 height: 15,
//                               ),
//                               Text(
//                                 'Home Audio',
//                                 style: TextStyle(
//                                     fontSize: 10, fontWeight: FontWeight.bold),
//                                 textAlign: TextAlign.center,
//                               ),
//                             ],
//                           ),
//                           // child: ListTile(
//                           // title: CircleAvatar(
//                           // radius: 100,
//                           // backgroundImage: AssetImage('assets/images/h5.jpg',),
//                           // ),
//                           // subtitle: Text('Home Audio',
//                           // style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
//                           // textAlign: TextAlign.center,
//                           // ),
//                           // ),
//                         ),
//                         Container(
//                           height: 85.0,
//                           width: 101.0,
//                           //padding: EdgeInsets.all(10),
//                           margin: EdgeInsets.only(bottom: 20.0),
//                           // margin: EdgeInsets.all(10),
//                           alignment: Alignment.topCenter,
//                           decoration: BoxDecoration(
//                             borderRadius: BorderRadius.circular(10),
//                           ),
//                           child: Column(
//                             children: <Widget>[
//                               Container(
//                                 height: 50.0,
//                                 width: 86.0,
//                                 child: ListTile(
//                                   title: CircleAvatar(
//                                     radius: 100,
//                                     backgroundImage: AssetImage(
//                                       'assets/images/t24.jpg',
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                               SizedBox(
//                                 height: 15,
//                               ),
//                               Text(
//                                 'Tv Accessories',
//                                 style: TextStyle(
//                                     fontSize: 10, fontWeight: FontWeight.bold),
//                                 textAlign: TextAlign.center,
//                               ),
//                             ],
//                           ),
//                           // child: ListTile(
//                           // title: CircleAvatar(
//                           // radius: 100,
//                           // backgroundImage: AssetImage('assets/images/t24.jpg',),
//                           // ),
//                           // subtitle: Text('Tv Accessories',
//                           //  style: TextStyle(fontSize: 10,fontWeight:FontWeight.bold),
//                           // textAlign: TextAlign.center,
//                           // ),
//                           // ),
//                         ),
//                         Container(
//                           height: 85.0,
//                           width: 100.0,
//                           //padding: EdgeInsets.all(10),
//                           margin: EdgeInsets.only(bottom: 20.0),
//                           //margin: EdgeInsets.all(10),
//                           alignment: Alignment.topCenter,
//                           decoration: BoxDecoration(
//                             borderRadius: BorderRadius.circular(10),
//                           ),
//                           child: Column(
//                             children: <Widget>[
//                               Container(
//                                 height: 50.0,
//                                 width: 86.0,
//                                 child: ListTile(
//                                   title: CircleAvatar(
//                                     radius: 100,
//                                     backgroundImage: AssetImage(
//                                       'assets/images/t24.jpg',
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                               SizedBox(
//                                 height: 15,
//                               ),
//                               Text(
//                                 'Tv Accessories',
//                                 style: TextStyle(
//                                     fontSize: 10, fontWeight: FontWeight.bold),
//                                 textAlign: TextAlign.center,
//                               ),
//                             ],
//                           ),
//                           //  child: ListTile(
//                           // title: CircleAvatar(
//                           // radius: 100,
//                           // backgroundImage: AssetImage('assets/images/p11.jpg',),
//                           // ),
//                           // subtitle: Text('Projectors',
//                           //  style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
//                           // textAlign: TextAlign.center,
//                           // ),
//                           // ),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//                 //Fisrt Stack show
//                 Stack(
//                   children: <Widget>[
//                     Container(
//                       //padding: EdgeInsets.all(10),
//                       //margin: EdgeInsets.all(10),
//                       child: SingleChildScrollView(
//                         //scrollDirection: Axis.horizontal,
//                         child: Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: <Widget>[
//                             InkWell(
//                               child: Container(
//                                 height: MediaQuery.of(context).size.height / 4,
//                                 width: MediaQuery.of(context).size.width / 4,
//                                 //padding: EdgeInsets.only(left:8),
//                                 margin: EdgeInsets.only(left: 20.0),
//                                 alignment: Alignment.topCenter,
//                                 //width:90,
//                                 //height:150,
//                                 decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.all(Radius.zero),
//                                   border: Border.all(
//                                     color: Color.fromRGBO(128, 196, 201, 2.0),
//                                   ),
//                                   color: Color.fromARGB(20, 209, 209, 209),
//                                 ),
//                                 child: Column(
//                                   children: <Widget>[
//                                     Image.asset(
//                                       'assets/images/t40.jpg',
//                                       fit: BoxFit.cover,
//                                       height: 60,
//                                       width: 55,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Text(
//                                       'Tv 55 Inch',
//                                       style: TextStyle(
//                                           fontSize: 12,
//                                           fontWeight: FontWeight.bold),
//                                       textAlign: TextAlign.center,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     // Text('Elements 55 Inch Tv',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                                     // ),
//                                     SizedBox(
//                                       height: 10,
//                                     ),
//                                     Text(
//                                       '10,592,500 L.L',
//                                       style: TextStyle(
//                                           fontSize: 11,
//                                           fontWeight: FontWeight.bold,
//                                           color: Colors.redAccent),
//                                       textAlign: TextAlign.center,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Icon(Icons.add_shopping_cart,
//                                         color: Colors.blue),
//                                   ],
//                                 ),
//                                 //child: MyArticles('assets/images/w6.jpeg',"TV-4K","\${700}"),
//                               ),
//                               onTap: () {
//                                 Navigator.push(
//                                     context,
//                                     MaterialPageRoute(
//                                         builder: (context) => Details()));
//                               },
//                             ),
//                             //End One
//                             SizedBox(
//                               width: 0,
//                             ),
//                             InkWell(
//                               child: Container(
//                                 height: MediaQuery.of(context).size.height / 4,
//                                 width: MediaQuery.of(context).size.width / 4,
//                                 //padding: EdgeInsets.only(right:6),
//                                 //padding: EdgeInsets.all(10),
//                                 margin:
//                                     EdgeInsets.only(right: 23.0, left: 12.0),
//                                 alignment: Alignment.topCenter,
//                                 //width:90,
//                                 //height:150,
//                                 //height:250,
//                                 //color: Colors.yellowAccent.shade400,
//                                 decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.all(Radius.zero),
//                                   border: Border.all(
//                                     color: Color.fromRGBO(128, 196, 201, 2.0),
//                                   ),
//                                   color: Color.fromARGB(20, 209, 209, 209),
//                                 ),
//                                 child: Column(
//                                   children: <Widget>[
//                                     Image.asset(
//                                       'assets/images/t40.jpg',
//                                       fit: BoxFit.cover,
//                                       height: 60,
//                                       width: 55,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Text(
//                                       'Tv 55 Inch',
//                                       style: TextStyle(
//                                           fontSize: 12,
//                                           fontWeight: FontWeight.bold),
//                                       textAlign: TextAlign.center,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     // Text('Elements 55 Inch Tv ',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                                     // ),
//                                     SizedBox(
//                                       height: 10,
//                                     ),
//                                     Text(
//                                       '9,366,000L.L',
//                                       style: TextStyle(
//                                           fontSize: 11,
//                                           fontWeight: FontWeight.bold,
//                                           color: Colors.redAccent),
//                                       textAlign: TextAlign.center,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Icon(Icons.add_shopping_cart,
//                                         color: Colors.blue),
//                                   ],
//                                 ),
//                               ),
//                               onTap: () {
//                                 Navigator.push(
//                                     context,
//                                     MaterialPageRoute(
//                                         builder: (context) => Apple()));
//                               },
//                             ),
//                             // end two
//                             InkWell(
//                               child: Container(
//                                 height: MediaQuery.of(context).size.height / 4,
//                                 width: MediaQuery.of(context).size.width / 4,
//                                 //padding: EdgeInsets.only(right:4),
//                                 //padding: EdgeInsets.all(10),
//                                 margin: EdgeInsets.only(right: 8.0),
//                                 alignment: Alignment.topCenter,
//                                 //width:90,
//                                 //height:150,
//                                 //height:250,
//                                 //color: Colors.yellowAccent.shade400,
//                                 decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.all(Radius.zero),
//                                   border: Border.all(
//                                     color: Color.fromRGBO(128, 196, 201, 2.0),
//                                   ),
//                                   color: Color.fromARGB(20, 209, 209, 209),
//                                 ),
//                                 child: Column(
//                                   children: <Widget>[
//                                     Image.asset(
//                                       'assets/images/t50.jpg',
//                                       fit: BoxFit.cover,
//                                       height: 60,
//                                       width: 75,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Text(
//                                       'Tv 55 Inch',
//                                       style: TextStyle(
//                                           fontSize: 12,
//                                           fontWeight: FontWeight.bold),
//                                       textAlign: TextAlign.left,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     // Text('Elements 55 Inch Breakless Tv',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                                     // ),
//                                     SizedBox(
//                                       height: 10,
//                                     ),
//                                     Text(
//                                       '9,254,500 L.L',
//                                       style: TextStyle(
//                                           fontSize: 11,
//                                           fontWeight: FontWeight.bold,
//                                           color: Colors.redAccent),
//                                       textAlign: TextAlign.center,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Icon(Icons.add_shopping_cart,
//                                         color: Colors.blue),
//                                   ],
//                                 ),
//                               ),
//                               onTap: () {},
//                             ),
//                             // end three
//                           ],
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//                 //End Fisrt Stack
//                 ///////////////////////
//                 //hon bedi jarb a3mlo hewe zato
//                 SizedBox(
//                   height: 10,
//                 ),
//                 Stack(
//                   children: <Widget>[
//                     Container(
//                       //padding: EdgeInsets.all(10),
//                       //margin: EdgeInsets.all(10),
//                       child: SingleChildScrollView(
//                         //scrollDirection: Axis.horizontal,
//                         child: Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: <Widget>[
//                             InkWell(
//                               child: Container(
//                                 height: MediaQuery.of(context).size.height / 4,
//                                 width: MediaQuery.of(context).size.width / 4,
//                                 //padding: EdgeInsets.only(left:8),
//                                 margin: EdgeInsets.only(left: 20.0),
//                                 alignment: Alignment.topCenter,
//                                 //width:90,
//                                 // height:150,
//                                 decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.all(Radius.zero),
//                                   border: Border.all(
//                                     color: Color.fromRGBO(128, 196, 201, 2.0),
//                                   ),
//                                   color: Color.fromARGB(20, 209, 209, 209),
//                                 ),
//                                 child: Column(
//                                   children: <Widget>[
//                                     Image.asset(
//                                       'assets/images/t40.jpg',
//                                       fit: BoxFit.cover,
//                                       height: 60,
//                                       width: 55,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Text(
//                                       'Tv 85 Inch',
//                                       style: TextStyle(
//                                           fontSize: 12,
//                                           fontWeight: FontWeight.bold),
//                                       textAlign: TextAlign.center,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     // Text('Elements 55 Inch Tv',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                                     // ),
//                                     SizedBox(
//                                       height: 10,
//                                     ),
//                                     Text(
//                                       '37,910,000 L.L',
//                                       style: TextStyle(
//                                           fontSize: 11,
//                                           fontWeight: FontWeight.bold,
//                                           color: Colors.redAccent),
//                                       textAlign: TextAlign.center,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Icon(Icons.add_shopping_cart,
//                                         color: Colors.blue),
//                                   ],
//                                 ),
//                                 //child: MyArticles('assets/images/w6.jpeg',"TV-4K","\${700}"),
//                               ),
//                               onTap: () {},
//                             ),
//                             //End One
//                             SizedBox(
//                               width: 0,
//                             ),
//                             InkWell(
//                               child: Container(
//                                 height: MediaQuery.of(context).size.height / 4,
//                                 width: MediaQuery.of(context).size.width / 4,
//                                 //padding: EdgeInsets.only(right:6),
//                                 //padding: EdgeInsets.all(10),
//                                 margin:
//                                     EdgeInsets.only(right: 23.0, left: 12.0),
//                                 alignment: Alignment.topCenter,
//                                 //width:90,
//                                 //height:150,
//                                 //height:250,
//                                 //color: Colors.yellowAccent.shade400,
//                                 decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.all(Radius.zero),
//                                   border: Border.all(
//                                     color: Color.fromRGBO(128, 196, 201, 2.0),
//                                   ),
//                                   color: Color.fromARGB(20, 209, 209, 209),
//                                 ),
//                                 child: Column(
//                                   children: <Widget>[
//                                     Image.asset(
//                                       'assets/images/t40.jpg',
//                                       fit: BoxFit.cover,
//                                       height: 60,
//                                       width: 55,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Text(
//                                       'Tv 24 Inch',
//                                       style: TextStyle(
//                                           fontSize: 12,
//                                           fontWeight: FontWeight.bold),
//                                       textAlign: TextAlign.center,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     // Text('Elements 55 Inch Tv ',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                                     // ),
//                                     SizedBox(
//                                       height: 10,
//                                     ),
//                                     Text(
//                                       '2,475,300L.L',
//                                       style: TextStyle(
//                                           fontSize: 11,
//                                           fontWeight: FontWeight.bold,
//                                           color: Colors.redAccent),
//                                       textAlign: TextAlign.center,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Icon(Icons.add_shopping_cart,
//                                         color: Colors.blue),
//                                   ],
//                                 ),
//                               ),
//                               onTap: () {},
//                             ),
//                             // end two
//                             InkWell(
//                               child: Container(
//                                 height: MediaQuery.of(context).size.height / 4,
//                                 width: MediaQuery.of(context).size.width / 4,
//                                 //padding: EdgeInsets.only(right:4),
//                                 //padding: EdgeInsets.all(10),
//                                 margin: EdgeInsets.only(right: 8.0),
//                                 alignment: Alignment.topCenter,
//                                 //width:90,
//                                 //height:150,
//                                 //height:250,
//                                 //color: Colors.yellowAccent.shade400,
//                                 decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.all(Radius.zero),
//                                   border: Border.all(
//                                     color: Color.fromRGBO(128, 196, 201, 2.0),
//                                   ),
//                                   color: Color.fromARGB(20, 209, 209, 209),
//                                 ),
//                                 child: Column(
//                                   children: <Widget>[
//                                     Image.asset(
//                                       'assets/images/t50.jpg',
//                                       fit: BoxFit.cover,
//                                       height: 60,
//                                       width: 75,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Text(
//                                       'Tv 40 Inch',
//                                       style: TextStyle(
//                                           fontSize: 12,
//                                           fontWeight: FontWeight.bold),
//                                       textAlign: TextAlign.left,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     // Text('Elements 55 Inch Breakless Tv',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                                     // ),
//                                     SizedBox(
//                                       height: 10,
//                                     ),
//                                     Text(
//                                       '5,352,500 L.L',
//                                       style: TextStyle(
//                                           fontSize: 11,
//                                           fontWeight: FontWeight.bold,
//                                           color: Colors.redAccent),
//                                       textAlign: TextAlign.center,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Icon(Icons.add_shopping_cart,
//                                         color: Colors.blue),
//                                   ],
//                                 ),
//                               ),
//                               onTap: () {},
//                             ),
//                             // end three
//                           ],
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//                 //5esl nafso
//                 //how tani nafso
//                 SizedBox(
//                   height: 10,
//                 ),
//                 Stack(
//                   children: <Widget>[
//                     Container(
//                       //padding: EdgeInsets.all(10),
//                       //margin: EdgeInsets.all(10),
//                       child: SingleChildScrollView(
//                         //scrollDirection: Axis.horizontal,
//                         child: Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: <Widget>[
//                             InkWell(
//                               child: Container(
//                                 height: MediaQuery.of(context).size.height / 4,
//                                 width: MediaQuery.of(context).size.width / 4,
//                                 //padding: EdgeInsets.only(left:8),
//                                 margin: EdgeInsets.only(left: 20.0),
//                                 alignment: Alignment.topCenter,
//                                 //width:90,
//                                 //height:150,
//                                 decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.all(Radius.zero),
//                                   border: Border.all(
//                                     color: Color.fromRGBO(128, 196, 201, 2.0),
//                                   ),
//                                   color: Color.fromARGB(20, 209, 209, 209),
//                                 ),
//                                 child: Column(
//                                   children: <Widget>[
//                                     Image.asset(
//                                       'assets/images/t40.jpg',
//                                       fit: BoxFit.cover,
//                                       height: 60,
//                                       width: 55,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Text(
//                                       'Tv 60 Inch',
//                                       style: TextStyle(
//                                           fontSize: 12,
//                                           fontWeight: FontWeight.bold),
//                                       textAlign: TextAlign.center,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     // Text('Elements 55 Inch Tv',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                                     // ),
//                                     SizedBox(
//                                       height: 10,
//                                     ),
//                                     Text(
//                                       '10,592,500 L.L',
//                                       style: TextStyle(
//                                           fontSize: 11,
//                                           fontWeight: FontWeight.bold,
//                                           color: Colors.redAccent),
//                                       textAlign: TextAlign.center,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Icon(Icons.add_shopping_cart,
//                                         color: Colors.blue),
//                                   ],
//                                 ),
//                                 //child: MyArticles('assets/images/w6.jpeg',"TV-4K","\${700}"),
//                               ),
//                               onTap: () {},
//                             ),
//                             //End One
//                             SizedBox(
//                               width: 0,
//                             ),
//                             InkWell(
//                               child: Container(
//                                 height: MediaQuery.of(context).size.height / 4,
//                                 width: MediaQuery.of(context).size.width / 4,
//                                 //padding: EdgeInsets.only(right:6),
//                                 //padding: EdgeInsets.all(10),
//                                 margin:
//                                     EdgeInsets.only(right: 23.0, left: 12.0),
//                                 alignment: Alignment.topCenter,
//                                 //width:90,
//                                 //height:150,
//                                 //height:250,
//                                 //color: Colors.yellowAccent.shade400,
//                                 decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.all(Radius.zero),
//                                   border: Border.all(
//                                     color: Color.fromRGBO(128, 196, 201, 2.0),
//                                   ),
//                                   color: Color.fromARGB(20, 209, 209, 209),
//                                 ),
//                                 child: Column(
//                                   children: <Widget>[
//                                     Image.asset(
//                                       'assets/images/t40.jpg',
//                                       fit: BoxFit.cover,
//                                       height: 60,
//                                       width: 55,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Text(
//                                       'Tv 65 Inch',
//                                       style: TextStyle(
//                                           fontSize: 12,
//                                           fontWeight: FontWeight.bold),
//                                       textAlign: TextAlign.center,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     // Text('Elements 55 Inch Tv ',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                                     // ),
//                                     SizedBox(
//                                       height: 10,
//                                     ),
//                                     Text(
//                                       '9,366,000L.L',
//                                       style: TextStyle(
//                                           fontSize: 11,
//                                           fontWeight: FontWeight.bold,
//                                           color: Colors.redAccent),
//                                       textAlign: TextAlign.center,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Icon(Icons.add_shopping_cart,
//                                         color: Colors.blue),
//                                   ],
//                                 ),
//                               ),
//                               onTap: () {},
//                             ),
//                             // end two
//                             InkWell(
//                               child: Container(
//                                 height: MediaQuery.of(context).size.height / 4,
//                                 width: MediaQuery.of(context).size.width / 4,
//                                 //padding: EdgeInsets.only(right:4),
//                                 //padding: EdgeInsets.all(10),
//                                 margin: EdgeInsets.only(right: 8.0),
//                                 alignment: Alignment.topCenter,
//                                 //width:90,
//                                 // height:150,
//                                 //height:250,
//                                 //color: Colors.yellowAccent.shade400,
//                                 decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.all(Radius.zero),
//                                   border: Border.all(
//                                     color: Color.fromRGBO(128, 196, 201, 2.0),
//                                   ),
//                                   color: Color.fromARGB(20, 209, 209, 209),
//                                 ),
//                                 child: Column(
//                                   children: <Widget>[
//                                     Image.asset(
//                                       'assets/images/t50.jpg',
//                                       fit: BoxFit.cover,
//                                       height: 60,
//                                       width: 75,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Text(
//                                       'Tv 70 Inch',
//                                       style: TextStyle(
//                                           fontSize: 12,
//                                           fontWeight: FontWeight.bold),
//                                       textAlign: TextAlign.left,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     // Text('Elements 55 Inch Breakless Tv',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                                     // ),
//                                     SizedBox(
//                                       height: 10,
//                                     ),
//                                     Text(
//                                       '9,254,500 L.L',
//                                       style: TextStyle(
//                                           fontSize: 11,
//                                           fontWeight: FontWeight.bold,
//                                           color: Colors.redAccent),
//                                       textAlign: TextAlign.center,
//                                     ),
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Icon(Icons.add_shopping_cart,
//                                         color: Colors.blue),
//                                   ],
//                                 ),
//                               ),
//                               onTap: () {},
//                             ),
//                             // end three
//                           ],
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//                 //5els hewe tani
//                 SizedBox(
//                   height: 10,
//                 ),
//               ],
//             );
//           } else {}
//         }),
//       ),
//     );
//   }
// }
