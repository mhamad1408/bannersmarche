import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:marche/data/response/config_model.dart';
import 'package:marche/model/arrivals.dart';
import 'package:marche/model/banners.dart';
import 'package:marche/model/brands.dart';
import 'package:marche/model/category.dart';
import 'package:marche/model/produts.dart';
import 'package:marche/model/specials.dart';
import 'package:marche/provider/home_provider/arrivals_provider.dart';
import 'package:marche/provider/home_provider/banner_provider.dart';
import 'package:marche/provider/home_provider/brand_provider.dart';
import 'package:marche/provider/home_provider/category_provider.dart';
import 'package:marche/provider/home_provider/specials_provider.dart';
import 'package:marche/screen/arrivalls/arrivalls.dart';
import 'package:marche/screen/brands/brands.dart';
import 'package:marche/screen/details/details.dart';
import 'package:marche/screen/offers/offers.dart';
// ignore: unused_import
import 'package:shared_preferences/shared_preferences.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  // ignore: unused_field
  Future<void> _loadData(BuildContext context, bool reload) async {
    await Provider.of<BannerProvider>(context, listen: false)
        .getBanners(context, reload);
  }

  List<Banners> items;
  List<BrandsModel> brand;
  List<CategoryModel> category;
  List<ProductModel> arrival;
  List<ProductModel> specials;
  // Brands Start Data
  Future<void> _loadsData(BuildContext context, bool reload) async {
    await Provider.of<BrandProvider>(context, listen: false)
        .getBrand(context, reload);
  }

  // Brands End Data
  //Category Start Data
  Future<void> _loadCaategory(BuildContext context, bool reload) async {
    await Provider.of<CategoryProvider>(context, listen: false)
        .getCategory(context, reload);
  }

  //Category End Data
  //Arrival Start Data
  Future<void> _loadArrival(BuildContext context, bool reload) async {
    await Provider.of<ArrivalProvider>(context, listen: false)
        .getArrival(context, reload);
  }

  //Arrival End Data
  // Specials Start Data
  Future<void> _loadSpecials(BuildContext context, bool reload) async {
    await Provider.of<SpecialsProvider>(context, listen: false)
        .getSpecials(context, reload);
  }

  //Specials End Data
  // Banners data;
  @override
  void initState() {
    _loadData(context, true);
    _loadsData(context, true);
    _loadCaategory(context, true);
    _loadArrival(context, true);
    _loadSpecials(context, true);
    super.initState();
    // databanners = getBanners();
  }

  @override
  Widget build(BuildContext context) {
    //context.read<BannerProvider>().getBanners;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.white,
        dividerColor: Colors.black,
      ),
      title: 'Marche',
      home: Scaffold(
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'assets/images/logo png.png',
                fit: BoxFit.cover,
                height: 100,
              ),
            ],
          ),
          centerTitle: true,
          elevation: 6,
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.search),
                color: Colors.red,
                onPressed: () {
                  showSearch(context: context, delegate: SearchData());
                }),
          ],
        ),
        body: ListView(
          scrollDirection: Axis.vertical,
          children: <Widget>[
            SizedBox(
              height: 2,
            ),
            Container(
              padding: EdgeInsets.all(10),
              height: 200.0,
              width: double.infinity,
              child: Consumer<BannerProvider>(
                builder: (context, bannerProvider, child) {
                  items = bannerProvider.bannnersList;
                  return items != null && items.length > 0
                      ? Carousel(
                          dotSize: 1.5,
                          dotSpacing: 10,
                          images: items
                              .map<Widget>((e) => Image.network(
                                    BaseUrls.filesUrl + e.bannerImg,
                                    fit: BoxFit.cover,
                                  ))
                              .toList())
                      : Center(child: CircularProgressIndicator());
                },
              ),
            ),
            Container(
              height: 150.0,
              width: double.infinity,
              //color: Colors.red,
              margin: EdgeInsets.only(bottom: 0.0),
              child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Consumer<CategoryProvider>(
                      builder: (context, categoryProvider, child) {
                    category = categoryProvider.categorList;
                    return category != null && category.length > 0
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: category
                                .map<Widget>((e) => Container(
                                      height: 85.0,
                                      width: 90.0,
                                      margin: EdgeInsets.only(
                                          bottom: 40.0, left: 20.0),
                                      alignment: Alignment.topCenter,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: ListTile(
                                        title: CircleAvatar(
                                          radius: 100,
                                          backgroundImage: AssetImage(
                                            'assets/images/m1.png',
                                          ),
                                          //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
                                        ),
                                        subtitle: Text(
                                          e.categoryName,
                                          style: TextStyle(
                                              fontSize: 11,
                                              fontWeight: FontWeight.bold),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      //child: Text(e.categoryName),
                                    ))
                                .toList())
                        : Center(child: CircularProgressIndicator());
                  })
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //   children: <Widget>[
                  //   ]
                  // ),
                  ),
            ),
            Container(
              height: 150.0,
              width: double.infinity,
              //color: Colors.blue,
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    //Mobile
                    Container(
                      height: 85.0,
                      width: 90.0,
                      margin: EdgeInsets.only(bottom: 40.0, left: 20.0),
                      // height: 100.0,
                      // width: 100.0,
                      //  padding: EdgeInsets.all(10),
                      //   margin: EdgeInsets.all(10),
                      alignment: Alignment.topCenter,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: ListTile(
                        title: CircleAvatar(
                          radius: 100,
                          backgroundImage: AssetImage(
                            'assets/images/m1.png',
                          ),
                          //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
                        ),
                        subtitle: Text(
                          'Mobiles',
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      // child: CircleAvatar(
                      //  radius: 40,
                      //  backgroundImage: AssetImage('assets/images/m1.png',),
                      //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
                      // ),
                    ),
                    //end Mobil
                    //Kitchen
                    Container(
                      height: 85.0,
                      width: 90.0,
                      margin: EdgeInsets.only(bottom: 40.0, left: 20.0),
                      // height: 100.0,
                      // width: 100.0,
                      //  padding: EdgeInsets.all(10),
                      //   margin: EdgeInsets.all(10),
                      alignment: Alignment.topCenter,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: ListTile(
                        title: CircleAvatar(
                          radius: 100,
                          backgroundImage: AssetImage(
                            'assets/images/k1.jpg',
                          ),
                        ),
                        subtitle: Text(
                          'Kitchen',
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    //End Kitchen
                    //Beauty
                    Container(
                      height: 85.0,
                      width: 90.0,
                      margin: EdgeInsets.only(bottom: 40.0, left: 20.0),
                      // height: 100.0,
                      // width: 100.0,
                      //  padding: EdgeInsets.all(10),
                      //   margin: EdgeInsets.all(10),
                      alignment: Alignment.topCenter,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: ListTile(
                        title: CircleAvatar(
                          radius: 100,
                          backgroundImage: AssetImage(
                            'assets/images/b3.jpg',
                          ),
                        ),
                        subtitle: Text(
                          'Boutique',
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    //End Beauty
                    //Hair care
                    Container(
                      height: 85.0,
                      width: 90.0,
                      margin: EdgeInsets.only(bottom: 40.0, left: 20.0),
                      // height:  100.0,
                      // width:  100.0,
                      //  padding: EdgeInsets.all(10),
                      //   margin: EdgeInsets.all(10),
                      alignment: Alignment.topCenter,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: ListTile(
                        title: CircleAvatar(
                          radius: 100,
                          backgroundImage: AssetImage(
                            'assets/images/h2.jpg',
                          ),
                        ),
                        subtitle: Text(
                          'Hair care',
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    //End Hair care
                    //Personal care
                    Container(
                      height: 85.0,
                      width: 95.0,
                      margin: EdgeInsets.only(bottom: 40.0, left: 20.0),
                      // height: 100.0,
                      // width: 100.0,
                      //  padding: EdgeInsets.all(10),
                      //   margin: EdgeInsets.all(10),
                      alignment: Alignment.topCenter,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: ListTile(
                        title: CircleAvatar(
                          radius: 100,
                          backgroundImage: AssetImage(
                            'assets/images/p2.png',
                          ),
                        ),
                        subtitle: Text(
                          'Personal care',
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    //End Personal care
                    //Cleaning
                    Container(
                      height: 85.0,
                      width: 90.0,
                      margin: EdgeInsets.only(bottom: 40.0, left: 20.0),
                      // height: 100.0,
                      // width: 100.0,
                      //  padding: EdgeInsets.all(10),
                      //   margin: EdgeInsets.all(10),
                      alignment: Alignment.topCenter,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: ListTile(
                        title: CircleAvatar(
                          radius: 100,
                          backgroundImage: AssetImage(
                            'assets/images/h3.jpg',
                          ),
                          //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
                        ),
                        subtitle: Text(
                          'Cleaning',
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      // child: CircleAvatar(
                      //  radius: 40,
                      //  backgroundImage: AssetImage('assets/images/h3.jpg',),
                      //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
                      // ),
                    ),
                    //End Cleaning
                    //Household
                    Container(
                      height: 85.0,
                      width: 95.0,
                      margin: EdgeInsets.only(bottom: 40.0, left: 20.0),
                      // height: 100.0,
                      // width: 102.0,
                      //  padding: EdgeInsets.all(10),
                      //   margin: EdgeInsets.all(10),
                      alignment: Alignment.topCenter,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: ListTile(
                        title: CircleAvatar(
                          radius: 100,
                          backgroundImage: AssetImage(
                            'assets/images/h4.jpg',
                          ),
                          //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
                        ),
                        subtitle: Text(
                          'Household',
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      // child: CircleAvatar(
                      //  radius: 40,
                      //  backgroundImage: AssetImage('assets/images/h4.jpg',),
                      //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
                      // ),
                    ),
                    //End Household
                    //Electronics
                    Container(
                      height: 85.0,
                      width: 95.0,
                      margin: EdgeInsets.only(bottom: 40.0, left: 20.0),
                      // height: 100.0,
                      // width: 106.0,
                      //  padding: EdgeInsets.all(10),
                      //   margin: EdgeInsets.all(10),
                      alignment: Alignment.topCenter,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: ListTile(
                        title: CircleAvatar(
                          radius: 100,
                          backgroundImage: AssetImage(
                            'assets/images/e2.jpeg',
                          ),
                          //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
                        ),
                        subtitle: Text(
                          'Electronics',
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      // child: CircleAvatar(
                      //  radius: 40,
                      //  backgroundImage: AssetImage('assets/images/e2.jpeg',),
                      //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
                      // ),
                    ),
                    //End Electronics
                    //Fashion
                    Container(
                      height: 85.0,
                      width: 90.0,
                      margin: EdgeInsets.only(bottom: 40.0, left: 20.0),
                      // height: 100.0,
                      // width: 100.0,
                      //  padding: EdgeInsets.all(10),
                      //   margin: EdgeInsets.all(10),
                      alignment: Alignment.topCenter,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: ListTile(
                        title: CircleAvatar(
                          radius: 100,
                          backgroundImage: AssetImage(
                            'assets/images/f3.png',
                          ),
                          //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
                        ),
                        subtitle: Text(
                          'Fashion',
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      // child: CircleAvatar(
                      //  radius: 40,
                      //  backgroundImage: AssetImage('assets/images/f3.png',),
                      //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
                      // ),
                    ),
                    //End Fashion
                    //Automotive
                    Container(
                      height: 85.0,
                      width: 90.0,
                      margin: EdgeInsets.only(bottom: 40.0, left: 20.0),
                      // height: 100.0,
                      // width: 104.0,
                      //  padding: EdgeInsets.all(10),
                      //   margin: EdgeInsets.all(10),
                      alignment: Alignment.topCenter,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: ListTile(
                        title: CircleAvatar(
                          radius: 100,
                          backgroundImage: AssetImage(
                            'assets/images/a1.jpg',
                          ),
                          //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
                        ),
                        subtitle: Text(
                          'Automotive',
                          style: TextStyle(
                              fontSize: 10, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      // child: CircleAvatar(
                      //  radius: 40,
                      //  backgroundImage: AssetImage('assets/images/a1.jpg',),
                      //  //child: Image.asset('assets/images/w1.jpg', fit: BoxFit.cover,),
                      // ),
                    ),
                    //End Automotive
                    //Kids
                    Container(
                      height: 85.0,
                      width: 90.0,
                      margin: EdgeInsets.only(bottom: 40.0, left: 20.0),
                      // height: 100.0,
                      // width: 100.0,
                      //  padding: EdgeInsets.all(10),
                      //   margin: EdgeInsets.all(10),
                      alignment: Alignment.topCenter,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: ListTile(
                        title: CircleAvatar(
                          radius: 100,
                          backgroundImage: AssetImage(
                            'assets/images/k2.jpg',
                          ),
                          //child: Image.asset('assets/images/t1.jpg', fit: BoxFit.cover,),
                        ),
                        subtitle: Text(
                          'Kids',
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    //End Kids
                  ],
                ),
              ),
            ),
            //End Categories
            //New Arrivalls
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10.0),
                  padding: EdgeInsets.all(10),
                  height: 50,
                  child: Text('New Arrivals',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: Colors.orange,
                          fontSize: 20,
                          fontWeight: FontWeight.w800)),
                ),
                Container(
                  margin: EdgeInsets.only(right: 10),
                  padding: EdgeInsets.all(10),
                  //height:40,
                  //width: 30,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black,
                    ),
                  ),
                  child: InkWell(
                    child: Text(
                      'VIEW ALL',
                      style: TextStyle(
                        color: Colors.blue,
                        fontSize: 10,
                      ),
                    ),
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Arrivalls()));
                    },
                  ),
                ),
              ],
            ),
            Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.all(10),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Consumer<ArrivalProvider>(
                      builder: (context, arrivalsProvider, child) {
                        arrival = arrivalsProvider.arrivalList;
                        return arrival != null && arrival.length > 0
                            ? Row(
                                children: arrival
                                    .map<Widget>((e) => InkWell(
                                          child: Container(
                                            padding: EdgeInsets.all(10),
                                            margin: EdgeInsets.all(10),
                                            alignment: Alignment.topCenter,
                                            width: 160,
                                            height: 195,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.all(Radius.zero),
                                              border: Border.all(
                                                color: Colors.black,
                                              ),
                                            ),
                                            child: Column(
                                              children: [
                                                Container(
                                                  height: 100,
                                                  child: Image.network(
                                                    BaseUrls.filesUrl +
                                                        e.slides.first
                                                            .productImageImg,
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                                Spacer(),
                                                Container(
                                                  height: 12,
                                                  // width: 100,
                                                  //alignment: Alignment.center,
                                                  //padding: EdgeInsets.all(10),
                                                  margin: EdgeInsets.all(10),
                                                  child: Text(
                                                    e.productName,
                                                    style: TextStyle(
                                                        fontSize: 10,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                                Spacer(),
                                                // SizedBox(
                                                //   height: 5,
                                                // ),
                                                Text(
                                                  e.productPrice.toString(),
                                                  style: TextStyle(
                                                      color: Colors.red),
                                                ),
                                                Spacer(),
                                                // SizedBox(
                                                //   height: 5,
                                                // ),
                                                Icon(Icons.add_shopping_cart,
                                                    color: Colors.blue),
                                              ],
                                            ),
                                          ),
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        Details(product: e)));
                                          },
                                        ))
                                    .toList())
                            : Center(child: CircularProgressIndicator());
                      },
                    ),
                  ),
                ),
              ],
            ),
            //Special Offers
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10.0),
                  padding: EdgeInsets.all(10),
                  height: 50,
                  child: Text('Special Offers',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: Colors.orange,
                          fontSize: 20,
                          fontWeight: FontWeight.w800)),
                ),
                Container(
                  margin: EdgeInsets.only(right: 10),
                  padding: EdgeInsets.all(10),

                  //height:40,
                  //width: 30,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black,
                    ),
                  ),
                  child: InkWell(
                    child: Text(
                      'VIEW ALL',
                      style: TextStyle(
                        color: Colors.blue,
                        fontSize: 10,
                      ),
                    ),
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Offers()));
                    },
                  ),
                ),
              ],
            ),
            Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.all(10),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Consumer<SpecialsProvider>(
                      builder: (context, specialsProvider, child) {
                        specials = specialsProvider.specialsList;
                        return specials != null && specials.length > 0
                            ? Row(
                                children: specials
                                    .map<Widget>((e) => Container(
                                          padding: EdgeInsets.all(10),
                                          margin: EdgeInsets.all(10),
                                          alignment: Alignment.topCenter,
                                          width: 150,
                                          height: 190,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.all(Radius.zero),
                                            border: Border.all(
                                              color: Colors.black,
                                            ),
                                          ),
                                          child: Column(
                                            children: [
                                              Container(
                                                height: 100,
                                                child: Image.network(
                                                  BaseUrls.filesUrl +
                                                      e.slides.first
                                                          .productImageImg,
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 5,
                                              ),
                                              Text(
                                                e.productName,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              Text(
                                                e.productPrice.toString(),
                                                style: TextStyle(
                                                    color: Colors.red),
                                              ),
                                              SizedBox(
                                                height: 5,
                                              ),
                                              Icon(Icons.add_shopping_cart,
                                                  color: Colors.blue),
                                            ],
                                          ),
                                        ))
                                    .toList())
                            : specials == null && specials.length == 0
                                ? Center(child: CircularProgressIndicator())
                                : Text('No Special Offers found !');
                      },
                    ),
                  ),
                ),
              ],
            ),
            //here Brand Start
            Container(
              height: 80,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10.0),
                    padding: EdgeInsets.all(10),
                    height: 40,
                    child: Text('Brands',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 20,
                            fontWeight: FontWeight.w800)),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 10),
                    padding: EdgeInsets.all(10),

                    //height:40,
                    //width: 30,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black,
                      ),
                    ),
                    child: InkWell(
                      child: Text(
                        'VIEW ALL',
                        style: TextStyle(
                          color: Colors.blue,
                          fontSize: 10,
                        ),
                      ),
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Brands()));
                      },
                    ),
                  ),
                ],
              ),
            ),
            Stack(
              children: <Widget>[
                Container(
                  height: 150,
                  width: 400,
                  padding: EdgeInsets.all(5),
                  margin: EdgeInsets.all(5),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Consumer<BrandProvider>(
                      builder: (context, brandProvider, child) {
                        brand = brandProvider.brandList;
                        return brand != null && items.length > 0
                            ? Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: brand
                                    .map<Widget>((e) => Container(
                                          height: 100,
                                          width: 200,
                                          child: Image.network(
                                            BaseUrls.filesUrl + e.brandImg,
                                            fit: BoxFit.cover,
                                          ),
                                        ))
                                    .toList())
                            : Center(child: CircularProgressIndicator());
                      },
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class SearchData extends SearchDelegate<String> {
  @override
  List<Widget> buildActions(BuildContext context) {
    //  buildActions
    return [
      IconButton(icon: Icon(Icons.clear), onPressed: () {}),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    //  buildLeading
    return IconButton(icon: Icon(Icons.arrow_back), onPressed: () {});
  }

  @override
  Widget buildResults(BuildContext context) {
    //  buildResults
    return null;
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // ignore: todo
    // TODO: buildSuggestions
    return Text('Search');
  }
}

// class CarouselImage extends StatelessWidget {
//   Future<void> _loadData(BuildContext context, bool reload) async {
//     await Provider.of<BannerProvider>(context, listen: false)
//         .getBanners(context, reload);
//   }

//   CarouselImage({
//     Key key,
//     @required this.map,
//   }) : super(key: key);
//   final Map<String, dynamic> map;
//   Banners data;
//   @override
//   Widget build(BuildContext context) {
//     _loadData(context, true);
//     return Padding(
//       padding: const EdgeInsets.all(8.0),
//       child: Carousel(
//         images: [
//           Image.network('${map['bannerImg']}'),
//         ],
//       ),
//     );
//   }
// }

//const CarouselImage({Key  key, @required this.map,}) : super(key: key);
// final Map<String, dynamic> map;
// @override
// Widget build(BuildContext context) {
//   return Padding(
//     padding: const EdgeInsets.all(8.0),
//     child: Carousel(
//      images:[
//       Image.network('${map['bannerImg']}'),
//      ],
//    ),
//   );
