import 'package:flutter/material.dart';
import 'package:marche/model/category.dart';
import 'package:marche/provider/home_provider/category_provider.dart';
import 'package:marche/screen/sandwiches/sandwiches.dart';
import 'package:provider/provider.dart';

class Bakery extends StatefulWidget {
  final CategoryModel categoryModel;
  Bakery({@required this.categoryModel});

  @override
  _BakeryState createState() => _BakeryState();
}

class _BakeryState extends State<Bakery> {
  // List<CategoryModel> category;
  //Category Start Data
  // Future<void> _loadCaategory(BuildContext context, bool reload) async {
  //   await Provider.of<CategoryProvider>(context, listen: false)
  //       .getCategory(context, reload);
  // }

  //Category End Data
  @override
  void initState() {
    // _loadCaategory(context, true);
    super.initState();
    // databanners = getBanners();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          SizedBox(
            height: 10,
          ),
          Container(
            height: 150,
            //padding: EdgeInsets.all(8),
            margin: EdgeInsets.all(8),
            child: Image.asset('assets/images/gh.jpg', fit: BoxFit.cover),
          ),
          Container(
            padding: EdgeInsets.only(left: 10),
            // padding: EdgeInsets.all(6),
            //margin: EdgeInsets.all(10),
            //alignment: Alignment.topRight,
            child: Text(
              'Shop by Category',
              style: TextStyle(
                color: Colors.blueAccent,
                fontSize: 16,
                //fontWeight: FontWeight.bold
              ),
            ),
          ),
          Container(child: Consumer<CategoryProvider>(
              builder: (context, categoryProvider, child) {
            // category = categoryProvider.categorList;
            return widget.categoryModel != null &&
                    widget.categoryModel.subcategories.length > 0
                ? GridView.count(
                    crossAxisCount: 2,
                    padding: const EdgeInsets.all(20),
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    physics:
                        NeverScrollableScrollPhysics(), // to disable GridView's scrolling
                    shrinkWrap: true, // You won't see infinite size error
                    children: widget.categoryModel.subcategories
                        .map<Widget>((e) => Container(
                              height: 250,
                              child: Stack(
                                children: <Widget>[
                                  Container(
                                    height: 70,
                                    decoration: BoxDecoration(
                                      border: Border.all(),
                                    ),
                                    child: Image.asset(
                                      'assets/images/ba.jpg',
                                      fit: BoxFit.cover,
                                      width: 120,
                                    ),
                                  ),
                                  InkWell(
                                    child: Container(
                                      alignment: Alignment.bottomCenter,
                                      height: 90,
                                      child: Text(
                                        e.subcategoryName,
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 11,
                                          // fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => Sandwiches(
                                                    subcategoryId:
                                                        e.subcategoryId,
                                                  )));
                                    },
                                  ),
                                ],
                              ),
                            ))
                        .toList())
                : Center(child: CircularProgressIndicator());
          })),
        ],
      ),
    );
  }
}
// class Bakery extends StatelessWidget {
//     //Category Start Data
//   Future<void> _loadCaategory(BuildContext context, bool reload) async {
//     await Provider.of<CategoryProvider>(context, listen: false)
//         .getCategory(context, reload);
//   }
 
//   //Category End Data
//   @override
//   Widget build(BuildContext context) => Scaffold(
//   body: ListView(
//   children: <Widget>[
//    SizedBox(
//   height: 10,
//   ),
//   // Container(
//   // color: Colors.teal,
//   // child: Center(
//   // child: Text('Bakery',
//   //  style: TextStyle(
//   //   fontSize: 20,
//   //   color: Colors.white,
//   //   fontWeight: FontWeight.bold,
//   //   ),
//   // ),
//   // ),
//   // ),
//   // Container For 2 image 
//   Container(
//     height: 150,
//     //padding: EdgeInsets.all(8),
//     margin: EdgeInsets.all(8),
//   child: Image.asset('assets/images/gh.jpg',fit: BoxFit.cover),
//   ),
//   //  Container(
//   //    height: 150,
//   //    padding: EdgeInsets.all(8),
//   //    margin: EdgeInsets.all(8),
//   // child: Image.asset('assets/images/gh.jpg',fit: BoxFit.cover),
//   // ),
//   Container(
//   padding: EdgeInsets.only(left:10),
//   // padding: EdgeInsets.all(6),
//    //margin: EdgeInsets.all(10),
//   //alignment: Alignment.topRight,
//   child: Text('Shop by Category',
//   style: TextStyle(
//   color: Colors.blueAccent,
//   fontSize: 16,
//   //fontWeight: FontWeight.bold
//   ),
//   ),
//   ),
//   //End Container for 2 images
//       GridView.count(
//       crossAxisCount: 2,
//       primary: false,
//       padding: const EdgeInsets.all(20),
//       crossAxisSpacing: 10,
//       mainAxisSpacing: 10,
//       physics: NeverScrollableScrollPhysics(), // to disable GridView's scrolling
//       shrinkWrap: true, // You won't see infinite size error
//       children: <Widget>[
//         Container(
//           height: 250,
//           //width: 200,
//           //color: Colors.green,
//           child: Stack(
//           children: <Widget>[
//           Container(
//           height: 70,
//           decoration: BoxDecoration(
//           border: Border.all(),
//           ),
//           child: Image.asset('assets/images/ba.jpg',fit: BoxFit.cover,width: 120,
//           ),
//           ),
//            InkWell(
//             child: Container(
//             alignment: Alignment.bottomCenter,
//           height: 90,
//           child: Text('Sandwiches',
//           style: TextStyle(
//           color: Colors.black,
//           fontSize: 11,
//          // fontWeight: FontWeight.bold,
//           ),
//           ),
//           ),
//           onTap: (){
//           Navigator.push(context,MaterialPageRoute(builder:(context)=>Sandwiches()));
//           },
//            ),
//           ],
//           ),
//         ),
//         Container(
//           height: 250,
//           //color: Colors.blue,
//            child: Stack(
//           children: <Widget>[
//           Container(
//           height: 70,
//           decoration: BoxDecoration(
//           border: Border.all(),
//           ),
//           child: Image.asset('assets/images/pa.jpg',fit: BoxFit.cover,width: 120,
//           ),
//           ),
//           InkWell(
//              child: Container(
//              alignment: Alignment.bottomCenter,
//             height: 90,
//             child: Text('Pastry',
//             style: TextStyle(
//             color: Colors.black,
//             fontSize: 11,
//            // fontWeight: FontWeight.bold,
//             ),
//             ),
//             ),
//             onTap: (){
//                 Navigator.push(context,MaterialPageRoute(builder:(context)=>Pastry()));
//             },
//           ),
//           ],
//           ),
//         ),
//           Container(
//           height: 250,
//           //color: Colors.red,
//             child: Stack(
//           children: <Widget>[
//           Container(
//           height: 70,
//           decoration: BoxDecoration(
//           border: Border.all(),
//           ),
//           child: Image.asset('assets/images/ka.jpg',fit: BoxFit.cover,width: 120,),
//           ),
//            InkWell(
//              child: Container(
//              alignment: Alignment.bottomCenter,
//           height: 90,
//           child: Text('KaaK',
//           style: TextStyle(
//           color: Colors.black,
//           fontSize: 11,
//           //fontWeight: FontWeight.bold,
//           ),
//           ),
//           ),
//           onTap: (){
//              Navigator.push(context,MaterialPageRoute(builder:(context)=>KaaK()));
//           },
//            ),
//           ],
//           ),
//         ),
//           Container(
//           height: 250,
//           //color: Colors.blue,
//             child: Stack(
//           children: <Widget>[
//           Container(
//           height: 70,
//           width: 120,
//           decoration: BoxDecoration(
//           border: Border.all(),
//           ),
//           child: Image.asset('assets/images/ar.jpg',fit: BoxFit.cover,width: 120,
//           ),
//           ),
//            InkWell(
//           child: Container(
//           alignment: Alignment.bottomCenter,
//           height: 90,
//           child: Text('Arabic Bread',
//           style: TextStyle(
//           color: Colors.black,
//           fontSize: 11,
//           //fontWeight: FontWeight.bold,
//           ),
//           ),
//           ),
//           onTap: (){
//             Navigator.push(context,MaterialPageRoute(builder:(context)=>Bread()));
//           },
//            ),
//           ],
//           ),
//         ),
//           Container(
//           height: 250,
//           //color: Colors.blue,
//             child: Stack(
//           children: <Widget>[
//           Container(
//           height: 70,
//           decoration: BoxDecoration(
//           border: Border.all(),
//           ),
//           child: Image.asset('assets/images/gh.jpg',fit: BoxFit.cover,width: 120,
//           ),
//           ),
//            InkWell(
//            child: Container(
//           alignment: Alignment.bottomCenter,
//           height: 90,
//           child: Text('Ghraybe',
//           style: TextStyle(
//           color: Colors.black,
//           fontSize: 11,
//           //fontWeight: FontWeight.bold,
//           ),
//           ),
//           ),
//           onTap: (){
//              Navigator.push(context,MaterialPageRoute(builder:(context)=>Ghraybe()));
//           },
//            ),
//           ],
//           ),
//         ),
//           Container(
//           height: 250,
//           //color: Colors.blue,
//             child: Stack(
//           children: <Widget>[
//           Container(
//           height: 70,
//           decoration: BoxDecoration(
//           border: Border.all(),
//           ),
//           child: Image.asset('assets/images/to.jpg',fit: BoxFit.cover,width: 120,
//           ),
//           ),
//            InkWell(
//            child: Container(
//           alignment: Alignment.bottomCenter,
//           height: 90,
//           child: Text('Toasts',
//           style: TextStyle(
//           color: Colors.black,
//           fontSize: 11,
//          // fontWeight: FontWeight.bold,
//           ),
//           ),
//           ),
//           onTap: (){
//             Navigator.push(context,MaterialPageRoute(builder:(context)=>Toasts()));
//           },
//            ),
//           ],
//           ),
//         ),
//           Container(
//           height: 250,
//           //color: Colors.blue,
//             child: Stack(
//           children: <Widget>[
//           Container(
//           height: 70,
//           decoration: BoxDecoration(
//           border: Border.all(),
//           ),
//           child: Image.asset('assets/images/fr1.jpg',fit: BoxFit.cover,width: 120,
//           ),
//           ),
//            InkWell(
//            child: Container(
//           alignment: Alignment.bottomCenter,
//           height: 90,
//           child: Text('Freshly Baked',
//           style: TextStyle(
//           color: Colors.black,
//           fontSize: 11,
//          // fontWeight: FontWeight.bold,
//           ),
//           ),
//           ),
//           onTap: (){
//             Navigator.push(context,MaterialPageRoute(builder:(context)=>Freshly()));
//           },
//            ),
//           ],
//           ),
//         ),
//       ],
//     ),
//   ],
//   ),
//   );
// }