import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:marche/screen/home/home.dart';

class Toasts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/images/logo png.png',
              fit: BoxFit.cover,
              height: 100,
            ),
          ],
        ),
        centerTitle: true,
        elevation: 6,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.search),
              color: Colors.red,
              onPressed: () {
                showSearch(context: context, delegate: SearchData());
              }),
        ],
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          SizedBox(
            height: 10,
          ),
          // Container(
          // color: Colors.teal,
          // child: Center(
          // child: Text('Television',
          //  style: TextStyle(
          //   fontSize: 20,
          //   color: Colors.white,
          //   fontWeight: FontWeight.bold,
          //   ),
          // ),
          // ),
          // ),
          // Begin Carousel
          Container(
            //padding: EdgeInsets.all(10),
            height: 200.0,
            width: double.infinity,
            child: Carousel(
              images: [
                Image.asset(
                  'assets/images/sm1.jpeg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/sm2.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/sm3.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/sm4.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/sm5.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/sm6.jpg',
                  fit: BoxFit.cover,
                ),
              ],

              dotSpacing: 10,
              dotSize: 1.5,
              //dotIncreaseSize :0.5,
              dotColor: Colors.white,
              //dotBgColor: Colors.blue.withOpacity(0.5),
              //overlayShadow: true,
              //overlayShadowColors: Colors.blue,
            ),
          ),
          // End Carousel
// Beign Text
          Container(
            color: Colors.teal,
            child: Center(
              child: Text(
                'Samdwiches',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          //End Text
          //Show Item
          Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.all(10),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      InkWell(
                        child: Container(
                          padding: EdgeInsets.all(10),
                          margin: EdgeInsets.all(10),
                          alignment: Alignment.topCenter,
                          width: 150,
                          height: 250,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.zero),
                            border: Border.all(
                              color: Colors.black,
                            ),
                            //color: Colors.yellowAccent.shade400,
                          ),
                          child: Column(
                            children: <Widget>[
                              Image.asset(
                                'assets/images/t40.jpg',
                                fit: BoxFit.cover,
                                height: 100,
                                width: 50,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                'Smart Tv',
                                style: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                'Smart Tv',
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black54),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                '9,500 L.L',
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.redAccent),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Icon(Icons.add_shopping_cart, color: Colors.blue),
                            ],
                          ),
                          //child: MyArticles('assets/images/w6.jpeg',"TV-4K","\${700}"),
                        ),
                        onTap: () {},
                      ),
                      //End One
                      InkWell(
                        child: Container(
                          padding: EdgeInsets.all(10),
                          margin: EdgeInsets.all(10),
                          alignment: Alignment.topCenter,
                          width: 150,
                          height: 250,
                          //color: Colors.yellowAccent.shade400,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.zero),
                            border: Border.all(
                              color: Colors.black,
                            ),
                            //color: Colors.yellowAccent.shade400,
                          ),
                          child: Column(
                            children: <Widget>[
                              Image.asset(
                                'assets/images/t40.jpg',
                                fit: BoxFit.cover,
                                height: 100,
                                width: 50,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                'Smart Tv',
                                style: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                'Smart Tv ',
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black54),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                '6.500 L.L',
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.redAccent),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Icon(Icons.add_shopping_cart, color: Colors.blue),
                            ],
                          ),
                        ),
                        onTap: () {},
                      ),
                      // end two
                      InkWell(
                        child: Container(
                          padding: EdgeInsets.all(10),
                          margin: EdgeInsets.all(10),
                          alignment: Alignment.topCenter,
                          width: 150,
                          height: 250,
                          //color: Colors.yellowAccent.shade400,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.zero),
                            border: Border.all(
                              color: Colors.black,
                            ),
                            //color: Colors.yellowAccent.shade400,
                          ),
                          child: Column(
                            children: <Widget>[
                              Image.asset(
                                'assets/images/t50.jpg',
                                fit: BoxFit.cover,
                                height: 100,
                                width: 50,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                'Smart Tv',
                                style: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                'Smart Tv',
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black54),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                '6.500 L.L',
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.redAccent),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Icon(Icons.add_shopping_cart, color: Colors.blue),
                            ],
                          ),
                        ),
                        onTap: () {},
                      ),
                      // end three
                      InkWell(
                        child: Container(
                          padding: EdgeInsets.all(10),
                          margin: EdgeInsets.all(10),
                          alignment: Alignment.topCenter,
                          width: 150,
                          height: 250,
                          //color: Colors.yellowAccent.shade400,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.zero),
                            border: Border.all(
                              color: Colors.black,
                            ),
                            //color: Colors.yellowAccent.shade400,
                          ),
                          child: Column(
                            children: <Widget>[
                              Image.asset(
                                'assets/images/t51.jpg',
                                fit: BoxFit.cover,
                                height: 100,
                                width: 50,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                'Smart Tv',
                                style: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                'Smart Tv',
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black54),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                '6.500 L.L',
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.redAccent),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Icon(Icons.add_shopping_cart, color: Colors.blue),
                            ],
                          ),
                        ),
                        onTap: () {},
                      ),
                      // end for
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
