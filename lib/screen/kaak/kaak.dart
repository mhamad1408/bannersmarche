import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:marche/screen/home/home.dart';
class KaaK extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
     appBar: AppBar(
     title: Row(
     mainAxisAlignment: MainAxisAlignment.center,
     children: <Widget>[
     Image.asset('assets/images/logo png.png', fit:BoxFit.cover,height: 100,),
    ],
    ),
     centerTitle: true,
     elevation: 6,
     actions: <Widget>[
     IconButton(icon: Icon(Icons.search),color: Colors.red,onPressed: (){
     showSearch(context: context, delegate: SearchData());
    }),
     ],
  ),
   body: ListView(
   scrollDirection: Axis.vertical,
   children: <Widget>[
        SizedBox(
  height: 10,
  ),
  // Container(
  // color: Colors.teal,
  // child: Center(
  // child: Text('Television',
  //  style: TextStyle(
  //   fontSize: 20,
  //   color: Colors.white,
  //   fontWeight: FontWeight.bold,
  //   ),
  // ),
  // ),
  // ),
  // Begin Carousel
    Container(
         //padding: EdgeInsets.all(10),
            height: 200.0,
            width: double.infinity,
            child: Carousel(
              images: [
               Image.asset('assets/images/kak1.jpg', fit: BoxFit.cover,),
               Image.asset('assets/images/kak2.jpg', fit: BoxFit.cover,),
               Image.asset('assets/images/kak3.jpg', fit: BoxFit.cover,),
               Image.asset('assets/images/kak4.jpg', fit: BoxFit.cover,),
               Image.asset('assets/images/kak5.jpg', fit: BoxFit.cover,),
               Image.asset('assets/images/kak6.jpg', fit: BoxFit.cover,),
            ],

            dotSpacing: 10,
            dotSize :1.5,
            //dotIncreaseSize :0.5,
            dotColor: Colors.white,
            //dotBgColor: Colors.blue.withOpacity(0.5),
            //overlayShadow: true,
            //overlayShadowColors: Colors.blue,
            ),
         ),
  // End Carousel
// Beign Text
  Container(
  color: Colors.teal,
  child: Center(
  child: Text('Kaak',
   style: TextStyle(
    fontSize: 20,
    color: Colors.white,
    fontWeight: FontWeight.bold,
    ),
  ),
  ),
  ),
  //End Text
  //Show Item
  Stack(
  children: <Widget>[
    Container(
    padding: EdgeInsets.all(10),
    margin: EdgeInsets.all(10),
    child: SingleChildScrollView(
    scrollDirection: Axis.horizontal,
    child:Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
       InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
                child: Column(
                children: <Widget>[
                Image.asset('assets/images/kak7.jpg',fit:BoxFit.cover,height: 100,width: 100,),
                 SizedBox(
                height: 5,
                ),
                Text('Biscotti',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('Biscotti',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('60,000L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                 SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),               
                ],
              ),
              //child: MyArticles('assets/images/w6.jpeg',"TV-4K","\${700}"),
          ),
           onTap: (){},
        ),
        //End One
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
              //color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
              child: Column(
                children: <Widget>[
                Image.asset('assets/images/kak1.jpg',fit:BoxFit.cover,height: 100,width: 100,),
                SizedBox(
                height: 5,
                ),
                Text('kaak Balha',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('kaak Balha ',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('20,000L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                  SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),  
                ],
              ),
          ),
           onTap: (){},
        ),
        // end two
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
              //color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
              child: Column(
                children: <Widget>[
                Image.asset('assets/images/kak2.jpg',fit:BoxFit.cover,height: 100,width: 100,),
                SizedBox(
                height: 5,
                ),
                Text('kaak Al Hawa',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('kaak Al Hawa',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('20,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                  SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),  
                ],
              ),
          ),
           onTap: (){},
        ),
        // end three
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
              //color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
              child: Column(
                children: <Widget>[
                Image.asset('assets/images/kak3.jpg',fit:BoxFit.cover,height: 100,width: 100,),
                SizedBox(
                height: 5,
                ),
                Text('kaak Balha',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('kaak Balha',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('20,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                  SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),  
                ],
              ),
          ),
           onTap: (){},
        ),
        // end for
       InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
              //color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
              child: Column(
                children: <Widget>[
                Image.asset('assets/images/kak3.jpg',fit:BoxFit.cover,height: 100,width: 100,),
                SizedBox(
                height: 5,
                ),
                Text('kaak Super Extra',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('kaak Super Extra',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('28,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                  SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),  
                ],
              ),
          ),
           onTap: (){},
        ),
        // end five
    ],
    ),
  ),
  ),
  ],
  ),
    Stack(
  children: <Widget>[
    Container(
    padding: EdgeInsets.all(10),
    margin: EdgeInsets.all(10),
    child: SingleChildScrollView(
    scrollDirection: Axis.horizontal,
    child:Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
       InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
                child: Column(
                children: <Widget>[
                Image.asset('assets/images/kak6.jpg',fit:BoxFit.cover,height: 100,width: 100,),
                 SizedBox(
                height: 5,
                ),
                Text('kaak Cruhed',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('kaak Cruhed',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('20,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                 SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),               
                ],
              ),
              //child: MyArticles('assets/images/w6.jpeg',"TV-4K","\${700}"),
          ),
           onTap: (){},
        ),
        //End One
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
              //color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
              child: Column(
                children: <Widget>[
                Image.asset('assets/images/kak2.jpg',fit:BoxFit.cover,height: 100,width: 100,),
                SizedBox(
                height: 5,
                ),
                Text('Kaak Light',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('kaak Light ',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('28,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                  SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),  
                ],
              ),
          ),
           onTap: (){},
        ),
        // end two
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
              //color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
              child: Column(
                children: <Widget>[
                Image.asset('assets/images/kak2.jpg',fit:BoxFit.cover,height: 100,width: 100,),
                SizedBox(
                height: 5,
                ),
                Text('kaak Salted',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('kaak Salted',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('27,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                  SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),  
                ],
              ),
          ),
           onTap: (){},
        ),
        // end three
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
              //color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
              child: Column(
                children: <Widget>[
                Image.asset('assets/images/kak5.jpg',fit:BoxFit.cover,height: 100,width: 100,),
                SizedBox(
                height: 5,
                ),
                Text('kaak Salted Thyme',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('kaak Salted Thyme',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('27,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                  SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),  
                ],
              ),
          ),
           onTap: (){},
        ),
        // end for
       InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
              //color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
              child: Column(
                children: <Widget>[
                Image.asset('assets/images/kak2.jpg',fit:BoxFit.cover,height: 100,width: 100,),
                SizedBox(
                height: 5,
                ),
                Text('kaak Extra Boult',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('kaak Extra Boult',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('23,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                  SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),  
                ],
              ),
          ),
           onTap: (){},
        ),
        // end five
    ],
    ),
  ),
  ),
  ],
  ),
    Stack(
  children: <Widget>[
    Container(
    padding: EdgeInsets.all(10),
    margin: EdgeInsets.all(10),
    child: SingleChildScrollView(
    scrollDirection: Axis.horizontal,
    child:Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
       InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
                child: Column(
                children: <Widget>[
                Image.asset('assets/images/kak3.jpg',fit:BoxFit.cover,height: 100,width: 100,),
                 SizedBox(
                height: 5,
                ),
                Text('kaak Long Sesame',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('kaak Long Sesame',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('23,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                 SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),               
                ],
              ),
              //child: MyArticles('assets/images/w6.jpeg',"TV-4K","\${700}"),
          ),
           onTap: (){},
        ),
        //End One
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
              //color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
              child: Column(
                children: <Widget>[
                Image.asset('assets/images/kak2.jpg',fit:BoxFit.cover,height: 100,width: 100,),
                SizedBox(
                height: 5,
                ),
                Text('kaak Korchali',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('kaak Korchali',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('20,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                  SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),  
                ],
              ),
          ),
           onTap: (){},
        ),
        // end two
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
              //color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
              child: Column(
                children: <Widget>[
                Image.asset('assets/images/kak1.jpg',fit:BoxFit.cover,height: 100,width: 100,),
                SizedBox(
                height: 5,
                ),
                Text('kaak YoYo',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('kaak Yoyo',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('20,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                  SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),  
                ],
              ),
          ),
           onTap: (){},
        ),
        // end three
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
              //color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
              child: Column(
                children: <Widget>[
                Image.asset('assets/images/kak4.jpg',fit:BoxFit.cover,height: 100,width: 100,),
                SizedBox(
                height: 5,
                ),
                Text('kaak Oats',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('kaak Oats',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('20,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                  SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),  
                ],
              ),
          ),
           onTap: (){},
        ),
        // end for
       InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
              //color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
              child: Column(
                children: <Widget>[
                Image.asset('assets/images/kak2.jpg',fit:BoxFit.cover,height: 100,width: 100,),
                SizedBox(
                height: 5,
                ),
                Text('kaak Extra',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('kaak Extra',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('23,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                  SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),  
                ],
              ),
          ),
           onTap: (){},
        ),
        // end five
    ],
    ),
  ),
  ),
  ],
  ),
    Stack(
  children: <Widget>[
    Container(
    padding: EdgeInsets.all(10),
    margin: EdgeInsets.all(10),
    child: SingleChildScrollView(
    scrollDirection: Axis.horizontal,
    child:Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
       InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
                child: Column(
                children: <Widget>[
                Image.asset('assets/images/kak4.jpg',fit:BoxFit.cover,height: 100,width: 100,),
                 SizedBox(
                height: 5,
                ),
                Text('kaak Extra Sesame',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('kaak Extra Sesame',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('25,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                 SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),               
                ],
              ),
              //child: MyArticles('assets/images/w6.jpeg',"TV-4K","\${700}"),
          ),
           onTap: (){},
        ),
        //End One
         InkWell(
              child: Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              alignment: Alignment.topCenter,
              width:150,
              height:250,
              //color: Colors.yellowAccent.shade400,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.zero),
              border: Border.all(color:Colors.black,),
              //color: Colors.yellowAccent.shade400,
              ),
              child: Column(
                children: <Widget>[
                Image.asset('assets/images/kak2.jpg',fit:BoxFit.cover,height: 100,width: 100,),
                SizedBox(
                height: 5,
                ),
                Text('kaak Multicereal',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 5,
                ),
                Text('kaak Multicereal',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
                ),
                SizedBox(
                height: 10,
                ),
                Text('28,000 L.L',
                style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
                ),
                  SizedBox(
                height: 5,
                ),
                Icon(Icons.add_shopping_cart, color:Colors.blue),  
                ],
              ),
          ),
           onTap: (){},
        ),
        // end two
    ],
    ),
  ),
  ),
  ],
  ),
  ],
   ),
  );
  }
}