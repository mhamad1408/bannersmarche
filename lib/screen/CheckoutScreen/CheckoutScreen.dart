import 'package:flutter/material.dart';
import 'package:marche/model/cart.dart';
import 'package:marche/model/produts.dart';
import 'package:marche/provider/home_provider/cart_provider.dart';
import 'package:provider/provider.dart';

class CheckoutPage extends StatefulWidget {
  @override
  _CheckoutPageState createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  // Future<void> _produtsData(BuildContext context, bool reload) async {
  //   await Provider.of<ProductsProvider>(context, listen: false)
  //       .getProducts(context, reload, widget.subcaeegoryId);
  // }

  var _itemCount = 0;
  @override
  Widget build(BuildContext context) {
    Provider.of<CartProvider>(context, listen: false).getCartData();

    return Consumer<CartProvider>(
      builder: (context, cart, child) {
        return Scaffold(
            appBar: AppBar(
              title: Text('Checkout Page [\$ ${cart.total}]'),
            ),
            body: cart.cartList.length == 0
                ? Text('no items in your cart')
                : ListView.builder(
                    itemCount: cart.cartList.length,
                    itemBuilder: (context, index) {
                      return Card(
                        child: Stack(
                          children: [
                            Container(
                              child: Column(
                                children: <Widget>[
                                  // Container(
                                  // //alignment: Alignment.topCenter,
                                  // //height: 60,
                                  // //width: 60,
                                  // margin: EdgeInsets.only(right:100.0),
                                  // child:Text(cart.basketItems[index].title) ,
                                  // ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      // cart.cartList[index].product.slides !=
                                      //         null
                                      //     ? Container(
                                      //         height: 60,
                                      //         width: 100,
                                      //         child: Image.network(cart
                                      //             .cartList[index]
                                      //             .product
                                      //             .slides
                                      //             .first
                                      //             .productImageImg),
                                      //       )
                                      //     : Center(
                                      //         child:
                                      //             CircularProgressIndicator()),
                                      Container(
                                        child: Text(
                                          cart.cartList[index].product
                                              .productName,
                                          style: TextStyle(color: Colors.blue),
                                        ),
                                      ),
                                      Container(
                                        //alignment: Alignment.topCenter,
                                        //height: 60,
                                        //width: 60,
                                        child: Text(
                                            cart.cartList[index].product
                                                .productPrice
                                                .toString(),
                                            style:
                                                TextStyle(color: Colors.red)),
                                      ),
                                      Container(
                                        child: _itemCount != 0
                                            ? IconButton(
                                                icon: new Icon(Icons.remove),
                                                onPressed: () => {
                                                      Provider.of<CartProvider>(
                                                              context,
                                                              listen: false)
                                                          .setQuantity(
                                                              false,
                                                              cart.cartList[
                                                                  index])
                                                    })
                                            : new Container(),
                                      ),
                                      new Text(_itemCount.toString()),
                                      Container(
                                          child: IconButton(
                                              icon: new Icon(Icons.add),
                                              onPressed: () => {
                                                    Provider.of<CartProvider>(
                                                            context,
                                                            listen: false)
                                                        .setQuantity(
                                                            true,
                                                            cart.cartList[
                                                                index])
                                                  })),
                                      IconButton(
                                        icon: Icon(Icons.delete),
                                        onPressed: () {
                                          Provider.of<CartProvider>(context,
                                                  listen: false)
                                              .removeFromCart(
                                                  cart.cartList[index]);
                                        },
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            // ListTile(
                            //   title:Text(cart.basketItems[index].price.toString()),
                            //   leading: Image.asset(cart.basketItems[index].img) ,
                            //   subtitle:
                            //       Text(cart.basketItems[index].price.toString()),
                            //       trailing: Row(
                            //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            //       children: <Widget>[
                            //        IconButton(icon: new Icon(Icons.delete),onPressed: (){cart.remove(cart.basketItems[index]);}),
                            //        //new Text(_itemCount.toString()),
                            //        new IconButton(icon: new Icon(Icons.add),onPressed: (){cart.add(cart.basketItems[index]);}),
                            //       ],
                            //        ),
                            //   // trailing: IconButton(
                            //   //   icon: Icon(Icons.delete),
                            //   //   onPressed: () {
                            //   //     cart.remove(cart.basketItems[index]);
                            //   //   },
                            //   // ),
                            // ),
                          ],
                        ),
                      );
                    },
                  ));
      },
    );
  }
}
