import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:marche/data/response/config_model.dart';
import 'package:marche/model/cart.dart';
// ignore: unused_import
import 'package:marche/model/produts.dart';
import 'package:marche/provider/home_provider/products_provider.dart';
import 'package:marche/screen/details/details.dart';
import 'package:marche/screen/home/home.dart';
import 'package:provider/provider.dart';

class Sandwiches extends StatefulWidget {
  final int subcategoryId;

  const Sandwiches({@required this.subcategoryId});
  @override
  _SandwichesState createState() => _SandwichesState();
}

class _SandwichesState extends State<Sandwiches> {
  //List<ProductModel> produts;
  // Brands Start Data
  Future<void> _produtsData(BuildContext context, bool reload) async {
    await Provider.of<ProductsProvider>(context, listen: false).getProducts(
      context,
      reload,
      widget.subcategoryId,
    );
  }

  // Brands End Data
  @override
  void initState() {
    _produtsData(context, true);
    super.initState();
    // databanners = getBanners();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/images/logo png.png',
              fit: BoxFit.cover,
              height: 100,
            ),
          ],
        ),
        centerTitle: true,
        elevation: 6,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.search),
              color: Colors.red,
              onPressed: () {
                showSearch(context: context, delegate: SearchData());
              }),
        ],
      ),
      body: ListView(
        children: [
          SizedBox(
            height: 10,
          ),
          //   // Begin Carousel
          Container(
            //padding: EdgeInsets.all(10),
            height: 200.0,
            width: double.infinity,
            child: Carousel(
              images: [
                Image.asset(
                  'assets/images/bs1.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/bs3.jpeg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/bs5.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/bs9.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/bs12.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/bs14.jpg',
                  fit: BoxFit.cover,
                ),
              ],

              dotSpacing: 10,
              dotSize: 1.5,
              //dotIncreaseSize :0.5,
              dotColor: Colors.white,
              //dotBgColor: Colors.blue.withOpacity(0.5),
              //overlayShadow: true,
              //overlayShadowColors: Colors.blue,
            ),
          ),
//   // End Carousel
          Stack(
            children: [
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.all(10),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Consumer<ProductsProvider>(
                    builder: (context, productsProvider, child) {
                      return productsProvider.productList != null &&
                              productsProvider.productList.length > 0
                          ? Row(
                              children: productsProvider.productList
                                  .map<Widget>((e) => InkWell(
                                        child: Container(
                                          padding: EdgeInsets.all(10),
                                          margin: EdgeInsets.all(10),
                                          alignment: Alignment.topCenter,
                                          width: 150,
                                          height: 190,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.all(Radius.zero),
                                            border: Border.all(
                                              color: Colors.black,
                                            ),
                                          ),
                                          child: Column(
                                            children: [
                                              Container(
                                                height: 90,
                                                child: Image.network(
                                                  BaseUrls.filesUrl +
                                                      e.slides.first
                                                          .productImageImg,
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                              Text(
                                                e.productName,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              SizedBox(
                                                height: 5,
                                              ),
                                              Text(
                                                e.productPrice.toString(),
                                                style: TextStyle(
                                                    color: Colors.red),
                                              ),
                                              SizedBox(
                                                height: 5,
                                              ),
                                              Icon(Icons.add_shopping_cart,
                                                  color: Colors.blue),
                                            ],
                                          ),
                                        ),
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => Details(
                                                        product: e,
                                                      )));
                                        },
                                      ))
                                  .toList())
                          : Center(child: CircularProgressIndicator());
                    },
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
















// import 'package:carousel_pro/carousel_pro.dart';
// import 'package:flutter/material.dart';
// import 'package:marche/screen/home/home.dart';
// class Sandwiches extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//      appBar: AppBar(
//      title: Row(
//      mainAxisAlignment: MainAxisAlignment.center,
//      children: <Widget>[
//      Image.asset('assets/images/logo png.png', fit:BoxFit.cover,height: 100,),
//     ],
//     ),
//      centerTitle: true,
//      elevation: 6,
//      actions: <Widget>[
//      IconButton(icon: Icon(Icons.search),color: Colors.red,onPressed: (){
//      showSearch(context: context, delegate: SearchData());
//     }),
//      ],
//   ),
//    body: ListView(
//    scrollDirection: Axis.vertical,
//    children: <Widget>[
//         SizedBox(
//   height: 10,
//   ),
//   // Container(
//   // color: Colors.teal,
//   // child: Center(
//   // child: Text('Television',
//   //  style: TextStyle(
//   //   fontSize: 20,
//   //   color: Colors.white,
//   //   fontWeight: FontWeight.bold,
//   //   ),
//   // ),
//   // ),
//   // ),
//   // Begin Carousel
//     Container(
//          //padding: EdgeInsets.all(10),
//             height: 200.0,
//             width: double.infinity,
//             child: Carousel(
//               images: [
//                Image.asset('assets/images/bs1.jpg', fit: BoxFit.cover,),
//                Image.asset('assets/images/bs3.jpeg', fit: BoxFit.cover,),
//                Image.asset('assets/images/bs5.jpg', fit: BoxFit.cover,),
//                Image.asset('assets/images/bs9.jpg', fit: BoxFit.cover,),
//                Image.asset('assets/images/bs12.jpg', fit: BoxFit.cover,),
//                Image.asset('assets/images/bs14.jpg', fit: BoxFit.cover,),
//             ],

//             dotSpacing: 10,
//             dotSize :1.5,
//             //dotIncreaseSize :0.5,
//             dotColor: Colors.white,
//             //dotBgColor: Colors.blue.withOpacity(0.5),
//             //overlayShadow: true,
//             //overlayShadowColors: Colors.blue,
//             ),
//          ),
//   // End Carousel
// // Beign Text
//   Container(
//   color: Colors.teal,
//   child: Center(
//   child: Text('Sandwiches',
//    style: TextStyle(
//     fontSize: 20,
//     color: Colors.white,
//     fontWeight: FontWeight.bold,
//     ),
//   ),
//   ),
//   ),
//   //End Text
//   //Show Item
//   Stack(
//   children: <Widget>[
//     Container(
//     padding: EdgeInsets.all(10),
//     margin: EdgeInsets.all(10),
//     child: SingleChildScrollView(
//     scrollDirection: Axis.horizontal,
//     child:Row(
//     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//     children: <Widget>[
//        InkWell(
//               child: Container(
//               padding: EdgeInsets.all(10),
//               margin: EdgeInsets.all(10),
//               alignment: Alignment.topCenter,
//               width:150,
//               height:250,
//               decoration: BoxDecoration(
//               borderRadius: BorderRadius.all(Radius.zero),
//               border: Border.all(color:Colors.black,),
//               //color: Colors.yellowAccent.shade400,
//               ),
//                 child: Column(
//                 children: <Widget>[
//                 Image.asset('assets/images/bs2.jpg',fit:BoxFit.cover,height: 100,width: 100,),
//                  SizedBox(
//                 height: 5,
//                 ),
//                 Text('Pain Au Lait',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                 height: 5,
//                 ),
//                 Text('Pain Au Lait',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                 height: 10,
//                 ),
//                 Text('14,000 L.L',
//                 style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
//                 ),
//                  SizedBox(
//                 height: 5,
//                 ),
//                 Icon(Icons.add_shopping_cart, color:Colors.blue),               
//                 ],
//               ),
//               //child: MyArticles('assets/images/w6.jpeg',"TV-4K","\${700}"),
//           ),
//            onTap: (){},
//         ),
//         //End One
//          InkWell(
//               child: Container(
//               padding: EdgeInsets.all(10),
//               margin: EdgeInsets.all(10),
//               alignment: Alignment.topCenter,
//               width:150,
//               height:250,
//               //color: Colors.yellowAccent.shade400,
//               decoration: BoxDecoration(
//               borderRadius: BorderRadius.all(Radius.zero),
//               border: Border.all(color:Colors.black,),
//               //color: Colors.yellowAccent.shade400,
//               ),
//               child: Column(
//                 children: <Widget>[
//                 Image.asset('assets/images/bs4.jpg',fit:BoxFit.cover,height: 100,width: 100,),
//                 SizedBox(
//                 height: 5,
//                 ),
//                 Text('Pain Au Chocolat',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                 height: 5,
//                 ),
//                 Text('Pain Au Chocolat',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                 height: 10,
//                 ),
//                 Text('19,000 L.L',
//                 style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
//                 ),
//                   SizedBox(
//                 height: 5,
//                 ),
//                 Icon(Icons.add_shopping_cart, color:Colors.blue),  
//                 ],
//               ),
//           ),
//            onTap: (){},
//         ),
//         // end two
//          InkWell(
//               child: Container(
//               padding: EdgeInsets.all(10),
//               margin: EdgeInsets.all(10),
//               alignment: Alignment.topCenter,
//               width:150,
//               height:250,
//               //color: Colors.yellowAccent.shade400,
//               decoration: BoxDecoration(
//               borderRadius: BorderRadius.all(Radius.zero),
//               border: Border.all(color:Colors.black,),
//               //color: Colors.yellowAccent.shade400,
//               ),
//               child: Column(
//                 children: <Widget>[
//                 Image.asset('assets/images/bs6.jpg',fit:BoxFit.cover,height: 100,width: 100,),
//                 SizedBox(
//                 height: 5,
//                 ),
//                 Text('Sandwich Bun 25cm Sesame',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                 height: 5,
//                 ),
//                 Text('Sandwich Bun 25cm Sesame',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                 height: 10,
//                 ),
//                 Text('12,000 L.L',
//                 style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
//                 ),
//                   SizedBox(
//                 height: 5,
//                 ),
//                 Icon(Icons.add_shopping_cart, color:Colors.blue),  
//                 ],
//               ),
//           ),
//            onTap: (){},
//         ),
//         // end three
//          InkWell(
//               child: Container(
//               padding: EdgeInsets.all(10),
//               margin: EdgeInsets.all(10),
//               alignment: Alignment.topCenter,
//               width:150,
//               height:250,
//               //color: Colors.yellowAccent.shade400,
//               decoration: BoxDecoration(
//               borderRadius: BorderRadius.all(Radius.zero),
//               border: Border.all(color:Colors.black,),
//               //color: Colors.yellowAccent.shade400,
//               ),
//               child: Column(
//                 children: <Widget>[
//                 Image.asset('assets/images/bs7.png',fit:BoxFit.cover,height: 100,width: 100,),
//                 SizedBox(
//                 height: 5,
//                 ),
//                 Text('Sandwich Bun 25 cm',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                 height: 5,
//                 ),
//                 Text('Sandwich Bun 25 cm',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                 height: 10,
//                 ),
//                 Text('11,000 L.L',
//                 style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
//                 ),
//                   SizedBox(
//                 height: 5,
//                 ),
//                 Icon(Icons.add_shopping_cart, color:Colors.blue),  
//                 ],
//               ),
//           ),
//            onTap: (){},
//         ),
//         // end for
//          InkWell(
//               child: Container(
//               padding: EdgeInsets.all(10),
//               margin: EdgeInsets.all(10),
//               alignment: Alignment.topCenter,
//               width:150,
//               height:250,
//               //color: Colors.yellowAccent.shade400,
//               decoration: BoxDecoration(
//               borderRadius: BorderRadius.all(Radius.zero),
//               border: Border.all(color:Colors.black,),
//               //color: Colors.yellowAccent.shade400,
//               ),
//               child: Column(
//                 children: <Widget>[
//                 Image.asset('assets/images/bs8.jpg',fit:BoxFit.cover,height: 100,width: 100,),
//                 SizedBox(
//                 height: 5,
//                 ),
//                 Text('Sandwich Bun 20cm',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                 height: 5,
//                 ),
//                 Text('Sandwich Bun 20cm',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                 height: 10,
//                 ),
//                 Text('19,000 L.L',
//                 style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
//                 ),
//                   SizedBox(
//                 height: 5,
//                 ),
//                 Icon(Icons.add_shopping_cart, color:Colors.blue),  
//                 ],
//               ),
//           ),
//            onTap: (){},
//         ),
//         // end Five
//     ],
//     ),
//   ),
//   ),
//   ],
//   ),
//     Stack(
//   children: <Widget>[
//     Container(
//     padding: EdgeInsets.all(10),
//     margin: EdgeInsets.all(10),
//     child: SingleChildScrollView(
//     scrollDirection: Axis.horizontal,
//     child:Row(
//     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//     children: <Widget>[
//        InkWell(
//               child: Container(
//               padding: EdgeInsets.all(10),
//               margin: EdgeInsets.all(10),
//               alignment: Alignment.topCenter,
//               width:150,
//               height:250,
//               decoration: BoxDecoration(
//               borderRadius: BorderRadius.all(Radius.zero),
//               border: Border.all(color:Colors.black,),
//               //color: Colors.yellowAccent.shade400,
//               ),
//                 child: Column(
//                 children: <Widget>[
//                 Image.asset('assets/images/bs10.jpg',fit:BoxFit.cover,height: 100,width: 100,),
//                  SizedBox(
//                 height: 5,
//                 ),
//                 Text('American Burger Bun Sesame',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                 height: 5,
//                 ),
//                 Text('American Burger Bun Sesame',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                 height: 10,
//                 ),
//                 Text('20,000 L.L',
//                 style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
//                 ),
//                  SizedBox(
//                 height: 5,
//                 ),
//                 Icon(Icons.add_shopping_cart, color:Colors.blue),               
//                 ],
//               ),
//               //child: MyArticles('assets/images/w6.jpeg',"TV-4K","\${700}"),
//           ),
//            onTap: (){},
//         ),
//         //End One
//          InkWell(
//               child: Container(
//               padding: EdgeInsets.all(10),
//               margin: EdgeInsets.all(10),
//               alignment: Alignment.topCenter,
//               width:150,
//               height:250,
//               //color: Colors.yellowAccent.shade400,
//               decoration: BoxDecoration(
//               borderRadius: BorderRadius.all(Radius.zero),
//               border: Border.all(color:Colors.black,),
//               //color: Colors.yellowAccent.shade400,
//               ),
//               child: Column(
//                 children: <Widget>[
//                 Image.asset('assets/images/bs11.jpg',fit:BoxFit.cover,height: 100,width: 100,),
//                 SizedBox(
//                 height: 5,
//                 ),
//                 Text('Hamburge Multicereale',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                 height: 5,
//                 ),
//                 Text('Hamburge Multicereale',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                 height: 10,
//                 ),
//                 Text('23,000 L.L',
//                 style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
//                 ),
//                   SizedBox(
//                 height: 5,
//                 ),
//                 Icon(Icons.add_shopping_cart, color:Colors.blue),  
//                 ],
//               ),
//           ),
//            onTap: (){},
//         ),
//         // end two
//          InkWell(
//               child: Container(
//               padding: EdgeInsets.all(10),
//               margin: EdgeInsets.all(10),
//               alignment: Alignment.topCenter,
//               width:150,
//               height:250,
//               //color: Colors.yellowAccent.shade400,
//               decoration: BoxDecoration(
//               borderRadius: BorderRadius.all(Radius.zero),
//               border: Border.all(color:Colors.black,),
//               //color: Colors.yellowAccent.shade400,
//               ),
//               child: Column(
//                 children: <Widget>[
//                 Image.asset('assets/images/bs13.jpg',fit:BoxFit.cover,height: 100,width: 100,),
//                 SizedBox(
//                 height: 5,
//                 ),
//                 Text('Hamburger Bun Sesame',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                 height: 5,
//                 ),
//                 Text('Hamburger Bun Sesame',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                 height: 10,
//                 ),
//                 Text('17,000 L.L',
//                 style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
//                 ),
//                   SizedBox(
//                 height: 5,
//                 ),
//                 Icon(Icons.add_shopping_cart, color:Colors.blue),  
//                 ],
//               ),
//           ),
//            onTap: (){},
//         ),
//         // end three
//          InkWell(
//               child: Container(
//               padding: EdgeInsets.all(10),
//               margin: EdgeInsets.all(10),
//               alignment: Alignment.topCenter,
//               width:150,
//               height:250,
//               //color: Colors.yellowAccent.shade400,
//               decoration: BoxDecoration(
//               borderRadius: BorderRadius.all(Radius.zero),
//               border: Border.all(color:Colors.black,),
//               //color: Colors.yellowAccent.shade400,
//               ),
//               child: Column(
//                 children: <Widget>[
//                 Image.asset('assets/images/bs15.jpg',fit:BoxFit.cover,height: 100,width: 100,),
//                 SizedBox(
//                 height: 5,
//                 ),
//                 Text('American Burger Sesame',style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold),textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                 height: 5,
//                 ),
//                 Text('American Burger Sesame',style: TextStyle(fontSize: 14,fontWeight:FontWeight.bold,color: Colors.black54),textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                 height: 10,
//                 ),
//                 Text('22,000 L.L',
//                 style: TextStyle(fontSize: 12,fontWeight:FontWeight.bold,color: Colors.redAccent),textAlign: TextAlign.center,
//                 ),
//                   SizedBox(
//                 height: 5,
//                 ),
//                 Icon(Icons.add_shopping_cart, color:Colors.blue),  
//                 ],
//               ),
//           ),
//            onTap: (){},
//         ),
//         // end for
//     ],
//     ),
//   ),
//   ),
//   ],
//   ),
//   ],
//    ),
//   );
//   }
// }