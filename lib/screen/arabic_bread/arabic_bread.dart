// import 'package:carousel_pro/carousel_pro.dart';
// import 'package:flutter/material.dart';
// import 'package:marche/data/response/config_model.dart';
// import 'package:marche/model/category.dart';
// import 'package:marche/model/produts.dart';
// import 'package:marche/provider/home_provider/products_provider.dart';
// import 'package:marche/screen/details/details.dart';
// import 'package:marche/screen/home/home.dart';
// import 'package:provider/provider.dart';

// class ArabicBread extends StatefulWidget {
//   final CategoryModel categoryModel;
//   final ProductModel productModel;
//   const ArabicBread(
//       {@required this.categoryModel, @required this.productModel});
//   @override
//   _ArabicBreadState createState() => _ArabicBreadState();
// }

// class _ArabicBreadState extends State<ArabicBread> {
//   @override
//   void initState() {
//     // _loadCaategory(context, true);
//     super.initState();
//     // databanners = getBanners();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Row(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             Image.asset(
//               'assets/images/logo png.png',
//               fit: BoxFit.cover,
//               height: 100,
//             ),
//           ],
//         ),
//         centerTitle: true,
//         elevation: 6,
//         actions: <Widget>[
//           IconButton(
//               icon: Icon(Icons.search),
//               color: Colors.red,
//               onPressed: () {
//                 showSearch(context: context, delegate: SearchData());
//               }),
//         ],
//       ),
//       body: ListView(
//         children: [
//           SizedBox(
//             height: 10,
//           ),
//           Container(
//             //padding: EdgeInsets.all(10),
//             height: 200.0,
//             width: double.infinity,
//             child: Carousel(
//               images: [
//                 Image.asset(
//                   'assets/images/bs1.jpg',
//                   fit: BoxFit.cover,
//                 ),
//                 Image.asset(
//                   'assets/images/bs3.jpeg',
//                   fit: BoxFit.cover,
//                 ),
//                 Image.asset(
//                   'assets/images/bs5.jpg',
//                   fit: BoxFit.cover,
//                 ),
//                 Image.asset(
//                   'assets/images/bs9.jpg',
//                   fit: BoxFit.cover,
//                 ),
//                 Image.asset(
//                   'assets/images/bs12.jpg',
//                   fit: BoxFit.cover,
//                 ),
//                 Image.asset(
//                   'assets/images/bs14.jpg',
//                   fit: BoxFit.cover,
//                 ),
//               ],

//               dotSpacing: 10,
//               dotSize: 1.5,
//               //dotIncreaseSize :0.5,
//               dotColor: Colors.white,
//               //dotBgColor: Colors.blue.withOpacity(0.5),
//               //overlayShadow: true,
//               //overlayShadowColors: Colors.blue,
//             ),
//           ),
// //   // End Carousel
//         Stack(
//             children: [
//               Container(
//                 padding: EdgeInsets.all(10),
//                 margin: EdgeInsets.all(10),
//                 child: SingleChildScrollView(
//                   scrollDirection: Axis.horizontal,
//                   child: Consumer<ProductsProvider>(
//                     builder: (context, productsProvider, child) {
//                             return widget.productModel != null &&
//                     widget.productModel.productName.length > 0
//                           ? Row(
//                               children: widget.productModel
//                                   .map<Widget>((e) => InkWell(
//                                         child: Container(
//                                           padding: EdgeInsets.all(10),
//                                           margin: EdgeInsets.all(10),
//                                           alignment: Alignment.topCenter,
//                                           width: 150,
//                                           height: 190,
//                                           decoration: BoxDecoration(
//                                             borderRadius:
//                                                 BorderRadius.all(Radius.zero),
//                                             border: Border.all(
//                                               color: Colors.black,
//                                             ),
//                                           ),
//                                           child: Column(
//                                             children: [
//                                               Container(
//                                                 height: 90,
//                                                 child: Image.network(
//                                                   BaseUrls.filesUrl +
//                                                       e.slides.first
//                                                           .productImageImg,
//                                                   fit: BoxFit.cover,
//                                                 ),
//                                               ),
//                                               Text(
//                                                 e.productName,
//                                                 overflow: TextOverflow.ellipsis,
//                                                 style: TextStyle(
//                                                     fontSize: 12,
//                                                     fontWeight:
//                                                         FontWeight.bold),
//                                               ),
//                                               SizedBox(
//                                                 height: 5,
//                                               ),
//                                               Text(
//                                                 e.productPrice.toString(),
//                                                 style: TextStyle(
//                                                     color: Colors.red),
//                                               ),
//                                               SizedBox(
//                                                 height: 5,
//                                               ),
//                                               Icon(Icons.add_shopping_cart,
//                                                   color: Colors.blue),
//                                             ],
//                                           ),
//                                         ),
//                                         onTap: () {
//                                           Navigator.push(
//                                               context,
//                                               MaterialPageRoute(
//                                                   builder: (context) =>
//                                                       Details()));
//                                         },
//                                       ))
//                                   .toList())
//                           : Center(child: CircularProgressIndicator());
//                     },
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ],
//       ),
//     );
//   }
// }
