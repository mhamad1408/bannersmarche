import 'package:marche/model/produts.dart';

class CartModel {
  double _price;
  int _quantity;
  ProductModel _product;

  CartModel(double price, int quantity, ProductModel product) {
    this._price = price;
    this._quantity = quantity;
    this._product = product;
  }
  double get price => _price;
  int get qty => _quantity;
  set quantity(int qty) => _quantity = qty;
  ProductModel get product => _product;

  CartModel.fromJson(Map<String, dynamic> json) {
    _price = json['price'].toDouble();
    // productId = json['product_id'];
    _quantity = json['quantity'];
    if (json['prodduct'] != null) {
      _product = ProductModel.fromJson(json['product']);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['price'] = this._price;
    data['quantity'] = this._quantity;
    data['product'] = this._product.toJson();
    return data;
  }
}


// import 'package:flutter/material.dart';
// import 'package:marche/model/item.dart';
// import 'package:marche/model/produts.dart';

// class CartPovider extends ChangeNotifier {
//   List<Item> _items = [];
//   List<ProductModel> _productListitem = [];
//   double _totalPrice = 0.0;
//   void add(ProductModel product) {
//     _productListitem.add(product);
//     _totalPrice += product.productPrice;
//   }

//   // void add(Item item) {
//   //   _items.add(item);
//   //   _totalPrice += item.price;
//   //   notifyListeners();
//   // }
//   void remove(ProductModel product) {
//     _totalPrice -= product.productPrice;
//     _productListitem.remove(product);
//     notifyListeners();
//   }

//   // void remove(Item item) {
//   //   _totalPrice -= item.price;
//   //   _items.remove(item);
//   //   notifyListeners();
//   // }
//   int get count {
//     return _productListitem.length;
//   }

//   // int get count {
//   //   return _items.length;
//   // }
//   double get totalPrice {
//     return _totalPrice;
//   }

//   // double get totalPrice {
//   //   return _totalPrice;
//   // }
//   List<ProductModel> get basketItems {
//     return _productListitem;
//   }

//   // List<Item> get basketItems {
//   //   return _items;
//   // }

//   getItemTotal(List<ProductModel> productitems) {
//     double sum = 0.0;
//     productitems.forEach((e) {
//       sum += e.productId;
//     });
//     return "\$$sum";
//   }
// }
