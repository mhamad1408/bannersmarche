class Item {
  String title;
  double price;
  String description;
  String img;
  Item({this.title, this.price, this.img, this.description});
}