// class Specials {
//   int success;
//   List<Data> data;
//   String message;

//   Specials({this.success, this.data, this.message});

//   Specials.fromJson(Map<String, dynamic> json) {
//     success = json['success'];
//     if (json['data'] != null) {
//       data = new List<Data>();
//       json['data'].forEach((v) {
//         data.add(new Data.fromJson(v));
//       });
//     }
//     message = json['message'];
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['success'] = this.success;
//     if (this.data != null) {
//       data['data'] = this.data.map((v) => v.toJson()).toList();
//     }
//     data['message'] = this.message;
//     return data;
//   }
// }

import 'dart:convert';

class SpecialsModel {
  int productId;
  int brandId;
  String productName;
  String productDescr;
  String productQuantity;
  int productStatus;
  int productPrice;
  int productDiscount;
  int productUsd;
  int productPosition;
  String productExpiryDate;
  String productAddedDate;
  String productModifiedDate;
  List<Slides> slides;

  SpecialsModel(
      {this.productId,
      this.brandId,
      this.productName,
      this.productDescr,
      this.productQuantity,
      this.productStatus,
      this.productPrice,
      this.productDiscount,
      this.productUsd,
      this.productPosition,
      this.productExpiryDate,
      this.productAddedDate,
      this.productModifiedDate,
      this.slides});

  SpecialsModel.fromJson(Map<String, dynamic> json) {
    productId = json['product_id'];
    brandId = json['brand_id'];
    productName = json['product_name'];
    productDescr = json['product_descr'];
    productQuantity = json['product_quantity'];
    productStatus = json['product_status'];
    productPrice = json['product_price'];
    productDiscount = json['product_discount'];
    productUsd = json['product_usd'];
    productPosition = json['product_position'];
    productExpiryDate = json['product_expiry_date'];
    productAddedDate = json['product_added_date'];
    productModifiedDate = json['product_modified_date'];
    if (json['slides'] != null) {
      // ignore: deprecated_member_use
      slides = new List<Slides>();
      json['slides'].forEach((v) {
        slides.add(new Slides.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['product_id'] = this.productId;
    data['brand_id'] = this.brandId;
    data['product_name'] = this.productName;
    data['product_descr'] = this.productDescr;
    data['product_quantity'] = this.productQuantity;
    data['product_status'] = this.productStatus;
    data['product_price'] = this.productPrice;
    data['product_discount'] = this.productDiscount;
    data['product_usd'] = this.productUsd;
    data['product_position'] = this.productPosition;
    data['product_expiry_date'] = this.productExpiryDate;
    data['product_added_date'] = this.productAddedDate;
    data['product_modified_date'] = this.productModifiedDate;
    if (this.slides != null) {
      data['slides'] = this.slides.map((v) => v.toJson()).toList();
    }
    return data;
  }

  // ignore: non_constant_identifier_names
  List<SpecialsModel> SpecialsModelFromJson(String str) =>
      List<SpecialsModel>.from(
          json.decode(str).map((x) => SpecialsModel.fromJson(x)));
}

class Slides {
  String productImageImg;

  Slides({this.productImageImg});

  Slides.fromJson(Map<String, dynamic> json) {
    productImageImg = json['product_image_img'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['product_image_img'] = this.productImageImg;
    return data;
  }
}
