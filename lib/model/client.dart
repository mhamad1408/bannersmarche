class ClientModel {
  String name;
  String phone;
  String email;
  String password;
  String username;
  get getUsername => this.username;

  set setUsername(username) => this.username = username;

  get getEmail => this.email;

  set setEmail(email) => this.email = email;

  get getName => this.name;

  set setName(name) => this.name = name;

  get getPhone => this.phone;

  set setPhone(phone) => this.phone = phone;

  get getPassword => this.password;

  set setPassword(password) => this.password = password;

  ClientModel({this.phone, this.email, this.name, this.password});

  ClientModel.fromJson(Map<String, dynamic> json) {
    username = json['username'];
    name = json['name'];
    phone = json['phone'];
    email = json['email'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['password'] = this.password;
    return data;
  }
}
