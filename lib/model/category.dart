// class Categories {
//   int success;
//   List<Data> data;
//   String message;

//   Categories({this.success, this.data, this.message});

//   Categories.fromJson(Map<String, dynamic> json) {
//     success = json['success'];
//     if (json['data'] != null) {
//       data = new List<Data>();
//       json['data'].forEach((v) {
//         data.add(new Data.fromJson(v));
//       });
//     }
//     message = json['message'];
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['success'] = this.success;
//     if (this.data != null) {
//       data['data'] = this.data.map((v) => v.toJson()).toList();
//     }
//     data['message'] = this.message;
//     return data;
//   }
// }

import 'dart:convert';

class CategoryModel {
  int categoryId;
  String categoryName;
  int categoryStatus;
  int position;
  String categoryAddedDate;
  String categoryModifiedDate;
  List<Subcategories> subcategories;

  CategoryModel(
      {this.categoryId,
      this.categoryName,
      this.categoryStatus,
      this.position,
      this.categoryAddedDate,
      this.categoryModifiedDate,
      this.subcategories});

  CategoryModel.fromJson(Map<String, dynamic> json) {
    categoryId = json['category_id'];
    categoryName = json['category_name'];
    categoryStatus = json['category_status'];
    position = json['position'];
    categoryAddedDate = json['category_added_date'];
    categoryModifiedDate = json['category_modified_date'];
    if (json['subcategories'] != null) {
      // ignore: deprecated_member_use
      subcategories = new List<Subcategories>();
      json['subcategories'].forEach((v) {
        subcategories.add(new Subcategories.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['category_id'] = this.categoryId;
    data['category_name'] = this.categoryName;
    data['category_status'] = this.categoryStatus;
    data['position'] = this.position;
    data['category_added_date'] = this.categoryAddedDate;
    data['category_modified_date'] = this.categoryModifiedDate;
    if (this.subcategories != null) {
      data['subcategories'] =
          this.subcategories.map((v) => v.toJson()).toList();
    }
    return data;
  }

  // ignore: non_constant_identifier_names
  List<CategoryModel> CategoriesModelFromJson(String str) =>
      List<CategoryModel>.from(
          json.decode(str).map((x) => CategoryModel.fromJson(x)));
}

class Subcategories {
  int subcategoryId;
  int categoryId;
  String subcategoryName;
  int subcategoryPosition;
  int subcategoryStatus;
  String subcategoryAddedDate;
  String subcategoryModifiedDate;

  Subcategories(
      {this.subcategoryId,
      this.categoryId,
      this.subcategoryName,
      this.subcategoryPosition,
      this.subcategoryStatus,
      this.subcategoryAddedDate,
      this.subcategoryModifiedDate});

  Subcategories.fromJson(Map<String, dynamic> json) {
    subcategoryId = json['subcategory_id'];
    categoryId = json['category_id'];
    subcategoryName = json['subcategory_name'];
    subcategoryPosition = json['subcategory_position'];
    subcategoryStatus = json['subcategory_status'];
    subcategoryAddedDate = json['subcategory_added_date'];
    subcategoryModifiedDate = json['subcategory_modified_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['subcategory_id'] = this.subcategoryId;
    data['category_id'] = this.categoryId;
    data['subcategory_name'] = this.subcategoryName;
    data['subcategory_position'] = this.subcategoryPosition;
    data['subcategory_status'] = this.subcategoryStatus;
    data['subcategory_added_date'] = this.subcategoryAddedDate;
    data['subcategory_modified_date'] = this.subcategoryModifiedDate;
    return data;
  }

  // ignore: non_constant_identifier_names
  List<Subcategories> SubcategoriesFromJson(String str) =>
      List<Subcategories>.from(
          json.decode(str).map((x) => Subcategories.fromJson(x)));
}
