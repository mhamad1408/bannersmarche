// class InfoData {
//   int success;
//   Data data;
//   String message;

//   InfoData({this.success, this.data, this.message});

//   InfoData.fromJson(Map<String, dynamic> json) {
//     success = json['success'];
//     data = json['data'] != null ? new Data.fromJson(json['data']) : null;
//     message = json['message'];
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['success'] = this.success;
//     if (this.data != null) {
//       data['data'] = this.data.toJson();
//     }
//     data['message'] = this.message;
//     return data;
//   }
// }

import 'dart:convert';

class InfoDataModel {
  int infoId;
  String infoPhone;
  String googleKey;
  String infoWhatsapp;
  String infoEmail;
  String infoInstagram;
  String infoPassword;
  String infoAboutUs;
  String infoDollarRate;

  InfoDataModel(
      {this.infoId,
      this.infoPhone,
      this.googleKey,
      this.infoWhatsapp,
      this.infoEmail,
      this.infoInstagram,
      this.infoPassword,
      this.infoAboutUs,
      this.infoDollarRate});

  InfoDataModel.fromJson(Map<String, dynamic> json) {
    infoId = json['info_id'];
    infoPhone = json['info_phone'];
    googleKey = json['google_key'];
    infoWhatsapp = json['info_whatsapp'];
    infoEmail = json['info_email'];
    infoInstagram = json['info_instagram'];
    infoPassword = json['info_password'];
    infoAboutUs = json['info_about_us'];
    infoDollarRate = json['info_dollar_rate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['info_id'] = this.infoId;
    data['info_phone'] = this.infoPhone;
    data['google_key'] = this.googleKey;
    data['info_whatsapp'] = this.infoWhatsapp;
    data['info_email'] = this.infoEmail;
    data['info_instagram'] = this.infoInstagram;
    data['info_password'] = this.infoPassword;
    data['info_about_us'] = this.infoAboutUs;
    data['info_dollar_rate'] = this.infoDollarRate;
    return data;
  }

  // ignore: non_constant_identifier_names
  List<InfoDataModel> InfoDataModelFromJson(String str) =>
      List<InfoDataModel>.from(
          json.decode(str).map((x) => InfoDataModel.fromJson(x)));
}
