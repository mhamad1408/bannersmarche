// class Brands {
//   int success;
//   List<Data> data;
//   String message;

//   Brands({this.success, this.data, this.message});

//   Brands.fromJson(Map<String, dynamic> json) {
//     success = json['success'];
//     if (json['data'] != null) {
//       data = new List<Data>();
//       json['data'].forEach((v) {
//         data.add(new Data.fromJson(v));
//       });
//     }
//     message = json['message'];
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['success'] = this.success;
//     if (this.data != null) {
//       data['data'] = this.data.map((v) => v.toJson()).toList();
//     }
//     data['message'] = this.message;
//     return data;
//   }
// }

import 'dart:convert';

class BrandsModel {
  int brandId;
  String brandName;
  int brandPosition;
  String brandImg;
  int brandStatus;
  String brandAddedDate;
  String brandModifiedDate;

  BrandsModel(
      {this.brandId,
      this.brandName,
      this.brandPosition,
      this.brandImg,
      this.brandStatus,
      this.brandAddedDate,
      this.brandModifiedDate});

  BrandsModel.fromJson(Map<String, dynamic> json) {
    brandId = json['brand_id'];
    brandName = json['brand_name'];
    brandPosition = json['brand_position'];
    brandImg = json['brand_img'];
    brandStatus = json['brand_status'];
    brandAddedDate = json['brand_added_date'];
    brandModifiedDate = json['brand_modified_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['brand_id'] = this.brandId;
    data['brand_name'] = this.brandName;
    data['brand_position'] = this.brandPosition;
    data['brand_img'] = this.brandImg;
    data['brand_status'] = this.brandStatus;
    data['brand_added_date'] = this.brandAddedDate;
    data['brand_modified_date'] = this.brandModifiedDate;
    return data;
  }

  // ignore: non_constant_identifier_names
  List<BrandsModel> BrandsModelFromJson(String str) => List<BrandsModel>.from(
      json.decode(str).map((x) => BrandsModel.fromJson(x)));
}
