import 'dart:convert';

// class Banners {
//   int success;
//   List<Banner> data;
//   String message;

//   Banners({this.success, this.data, this.message});

//   Banners.fromJson(Map<String, dynamic> json) {
//     success = json['success'];
//     if (json['data'] != null) {
//       // ignore: deprecated_member_use
//       data = new List<Banner>();
//       json['data'].forEach((v) {
//         data.add(new Banner.fromJson(v));
//       });
//     }
//     message = json['message'];
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['success'] = this.success;
//     if (this.data != null) {
//       data['data'] = this.data.map((v) => v.toJson()).toList();
//     }
//     data['message'] = this.message;
//     return data;
//   }
// }

class Banners {
  int bannerId;
  int productId;
  String bannerImg;
  Null bannerUrl;
  int bannerStatus;
  String bannerExpiredDate;
  String bannerAddedDate;
  String bannerModifiedDate;

  Banners(
      {this.bannerId,
      this.productId,
      this.bannerImg,
      this.bannerUrl,
      this.bannerStatus,
      this.bannerExpiredDate,
      this.bannerAddedDate,
      this.bannerModifiedDate});

  Banners.fromJson(Map<String, dynamic> json) {
    bannerId = json['banner_id'];
    // productId = json['product_id'];
    bannerImg = json['banner_img'];
    bannerUrl = json['banner_url'];
    bannerStatus = json['banner_status'];
    bannerExpiredDate = json['banner_expired_date'];
    bannerAddedDate = json['banner_added_date'];
    bannerModifiedDate = json['banner_modified_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['banner_id'] = this.bannerId;
    data['product_id'] = this.productId;
    data['banner_img'] = this.bannerImg;
    data['banner_url'] = this.bannerUrl;
    data['banner_status'] = this.bannerStatus;
    data['banner_expired_date'] = this.bannerExpiredDate;
    data['banner_added_date'] = this.bannerAddedDate;
    data['banner_modified_date'] = this.bannerModifiedDate;
    return data;
  }

  // ignore: non_constant_identifier_names
  List<Banners> DataFromJson(String str) =>
      List<Banners>.from(json.decode(str).map((x) => Banners.fromJson(x)));
}
