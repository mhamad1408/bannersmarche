import 'package:dio/dio.dart';
import 'package:marche/provider/home_provider/arrivals_provider.dart';
import 'package:marche/provider/home_provider/auth_provider.dart';
import 'package:marche/provider/home_provider/banner_provider.dart';
import 'package:marche/provider/home_provider/brand_provider.dart';
import 'package:marche/provider/home_provider/cart_provider.dart';
import 'package:marche/provider/home_provider/category_provider.dart';
import 'package:marche/provider/home_provider/infodata_provider.dart';
import 'package:marche/provider/home_provider/products_provider.dart';
import 'package:marche/provider/home_provider/related_products_provider.dart';
import 'package:marche/provider/home_provider/slides_provider.dart';
import 'package:marche/provider/home_provider/specials_provider.dart';
import 'package:marche/remote/dio/logging_interceptor.dart';
import 'package:marche/repsetories/Info_data_repo.dart';
import 'package:marche/repsetories/arrivals_repo.dart';
import 'package:marche/repsetories/auth_repo.dart';
import 'package:marche/repsetories/banner_repo.dart';
import 'package:marche/repsetories/brands_repo.dart';
import 'package:marche/repsetories/cart_repo.dart';
import 'package:marche/repsetories/category_repo.dart';
import 'package:marche/repsetories/products_repo.dart';
import 'package:marche/repsetories/related_products_repo.dart';
import 'package:marche/repsetories/slides_repo.dart';
import 'package:marche/repsetories/specials_repo.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'data/dio/dioClient.dart';
import 'data/response/config_model.dart';
import 'package:get_it/get_it.dart';

import 'model/cart.dart';

final sl = GetIt.instance;

Future<void> init() async {
  // Core
  sl.registerLazySingleton(() => DioClient(BaseUrls.ProductAPi, sl(),
      loggingInterceptor: sl(), sharedPreferences: sl()));

  // Repository

  sl.registerLazySingleton(() => CartRepo(sharedPreferences: sl()));
  sl.registerLazySingleton(() => BannerRepo(dioClient: sl()));
  sl.registerLazySingleton(() => BrandRepo(dioClient: sl()));
  sl.registerLazySingleton(() => CategoryRepo(dioClient: sl()));
  sl.registerLazySingleton(() => ArrivalRepo(dioClient: sl()));
  sl.registerLazySingleton(() => SpecialsRepo(dioClient: sl()));
  sl.registerLazySingleton(() => SlidesRepo(dioClient: sl()));
  sl.registerLazySingleton(() => InfoDataRepo(dioClient: sl()));
  sl.registerLazySingleton(() => ProductsRepo(dioClient: sl()));
  sl.registerLazySingleton(
      () => AuthRepo(dioClient: sl(), sharedPreferences: null));
  sl.registerLazySingleton(() => RelatedProductsRepo(dioClient: sl()));
  // sl.registerLazySingleton(() => ExfighterRepo(dioClient: sl()));

  // sl.registerLazySingleton(() => RouteRepo(dioClient: sl()));

  // sl.registerLazySingleton(() => AboutUsRepo(dioClient: sl()));

  // sl.registerLazySingleton(() => PushNotificationRepo(dioClient: sl()));

  // sl.registerLazySingleton(
  //     () => AuthRepo(dioClient: sl(), sharedPreferences: sl()));

  // Provider

  sl.registerFactory(() => BannerProvider(bannersRepo: sl()));
  sl.registerFactory(() => BrandProvider(brandRepo: sl()));
  sl.registerFactory(() => CategoryProvider(categoryRepo: sl()));
  sl.registerFactory(() => ArrivalProvider(arrivalRepo: sl()));
  sl.registerFactory(() => SpecialsProvider(specialsRepo: sl()));
  sl.registerFactory(() => SlidesProvider(slidesRepo: sl()));
  sl.registerFactory(() => InfoDataProvider(infoDataRepo: sl()));
  sl.registerFactory(() => ProductsProvider(productsRepo: sl()));
  sl.registerFactory(() => AuthProvider(authRepo: sl()));
  sl.registerFactory(() => CartProvider(cartRepo: sl()));
  sl.registerFactory(() => RelatedProdcutsProvider(relatedproductsRepo: sl()));
  // sl.registerFactory(() => ExfighterProvider(exfighterRepo: sl()));

  // sl.registerFactory(
  //     () => PushNotificationProvider(pushnotificationRepo: sl()));

  // sl.registerFactory(() => AboutUsProvider(aboutusRepo: sl()));

  // sl.registerFactory(() => RouteProvider(routeRepo: sl()));
  // sl.registerFactory(() => AuthProvider(authRepo: sl()));
  // sl.registerFactory(() => RouteMapProvider(routeRepo: sl()));

  // External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => Dio());
  sl.registerLazySingleton(() => LoggingInterceptor());

  // sharedPreferences.setBool(AppConstants.PUSH, true);
}
